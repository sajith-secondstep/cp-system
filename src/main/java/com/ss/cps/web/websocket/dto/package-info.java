/**
 * Data Access Objects used by WebSocket services.
 */
package com.ss.cps.web.websocket.dto;
