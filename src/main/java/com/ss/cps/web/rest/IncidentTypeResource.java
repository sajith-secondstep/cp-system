package com.ss.cps.web.rest;

import com.ss.cps.domain.IncidentType;
import com.ss.cps.repository.IncidentTypeRepository;
import com.ss.cps.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.ss.cps.domain.IncidentType}.
 */
@RestController
@RequestMapping("/api")
public class IncidentTypeResource {

    private final Logger log = LoggerFactory.getLogger(IncidentTypeResource.class);

    private static final String ENTITY_NAME = "incidentType";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final IncidentTypeRepository incidentTypeRepository;

    public IncidentTypeResource(IncidentTypeRepository incidentTypeRepository) {
        this.incidentTypeRepository = incidentTypeRepository;
    }

    /**
     * {@code POST  /incident-types} : Create a new incidentType.
     *
     * @param incidentType the incidentType to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new incidentType, or with status {@code 400 (Bad Request)} if the incidentType has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/incident-types")
    public ResponseEntity<IncidentType> createIncidentType(@RequestBody IncidentType incidentType) throws URISyntaxException {
        log.debug("REST request to save IncidentType : {}", incidentType);
        if (incidentType.getId() != null) {
            throw new BadRequestAlertException("A new incidentType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        IncidentType result = incidentTypeRepository.save(incidentType);
        return ResponseEntity.created(new URI("/api/incident-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /incident-types} : Updates an existing incidentType.
     *
     * @param incidentType the incidentType to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated incidentType,
     * or with status {@code 400 (Bad Request)} if the incidentType is not valid,
     * or with status {@code 500 (Internal Server Error)} if the incidentType couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/incident-types")
    public ResponseEntity<IncidentType> updateIncidentType(@RequestBody IncidentType incidentType) throws URISyntaxException {
        log.debug("REST request to update IncidentType : {}", incidentType);
        if (incidentType.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        IncidentType result = incidentTypeRepository.save(incidentType);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, incidentType.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /incident-types} : get all the incidentTypes.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of incidentTypes in body.
     */
    @GetMapping("/incident-types")
    public List<IncidentType> getAllIncidentTypes() {
        log.debug("REST request to get all IncidentTypes");
        return incidentTypeRepository.findAll();
    }

    /**
     * {@code GET  /incident-types/:id} : get the "id" incidentType.
     *
     * @param id the id of the incidentType to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the incidentType, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/incident-types/{id}")
    public ResponseEntity<IncidentType> getIncidentType(@PathVariable Long id) {
        log.debug("REST request to get IncidentType : {}", id);
        Optional<IncidentType> incidentType = incidentTypeRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(incidentType);
    }

    /**
     * {@code DELETE  /incident-types/:id} : delete the "id" incidentType.
     *
     * @param id the id of the incidentType to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/incident-types/{id}")
    public ResponseEntity<Void> deleteIncidentType(@PathVariable Long id) {
        log.debug("REST request to delete IncidentType : {}", id);
        incidentTypeRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
