/**
 * View Models used by Spring MVC REST controllers.
 */
package com.ss.cps.web.rest.vm;
