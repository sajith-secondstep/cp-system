package com.ss.cps.web.rest;

import com.ss.cps.domain.CaseStatus;
import com.ss.cps.repository.CaseStatusRepository;
import com.ss.cps.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.ss.cps.domain.CaseStatus}.
 */
@RestController
@RequestMapping("/api")
public class CaseStatusResource {

    private final Logger log = LoggerFactory.getLogger(CaseStatusResource.class);

    private static final String ENTITY_NAME = "caseStatus";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CaseStatusRepository caseStatusRepository;

    public CaseStatusResource(CaseStatusRepository caseStatusRepository) {
        this.caseStatusRepository = caseStatusRepository;
    }

    /**
     * {@code POST  /case-statuses} : Create a new caseStatus.
     *
     * @param caseStatus the caseStatus to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new caseStatus, or with status {@code 400 (Bad Request)} if the caseStatus has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/case-statuses")
    public ResponseEntity<CaseStatus> createCaseStatus(@RequestBody CaseStatus caseStatus) throws URISyntaxException {
        log.debug("REST request to save CaseStatus : {}", caseStatus);
        if (caseStatus.getId() != null) {
            throw new BadRequestAlertException("A new caseStatus cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CaseStatus result = caseStatusRepository.save(caseStatus);
        return ResponseEntity.created(new URI("/api/case-statuses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /case-statuses} : Updates an existing caseStatus.
     *
     * @param caseStatus the caseStatus to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated caseStatus,
     * or with status {@code 400 (Bad Request)} if the caseStatus is not valid,
     * or with status {@code 500 (Internal Server Error)} if the caseStatus couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/case-statuses")
    public ResponseEntity<CaseStatus> updateCaseStatus(@RequestBody CaseStatus caseStatus) throws URISyntaxException {
        log.debug("REST request to update CaseStatus : {}", caseStatus);
        if (caseStatus.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CaseStatus result = caseStatusRepository.save(caseStatus);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, caseStatus.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /case-statuses} : get all the caseStatuses.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of caseStatuses in body.
     */
    @GetMapping("/case-statuses")
    public List<CaseStatus> getAllCaseStatuses() {
        log.debug("REST request to get all CaseStatuses");
        return caseStatusRepository.findAll();
    }

    /**
     * {@code GET  /case-statuses/:id} : get the "id" caseStatus.
     *
     * @param id the id of the caseStatus to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the caseStatus, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/case-statuses/{id}")
    public ResponseEntity<CaseStatus> getCaseStatus(@PathVariable Long id) {
        log.debug("REST request to get CaseStatus : {}", id);
        Optional<CaseStatus> caseStatus = caseStatusRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(caseStatus);
    }

    /**
     * {@code DELETE  /case-statuses/:id} : delete the "id" caseStatus.
     *
     * @param id the id of the caseStatus to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/case-statuses/{id}")
    public ResponseEntity<Void> deleteCaseStatus(@PathVariable Long id) {
        log.debug("REST request to delete CaseStatus : {}", id);
        caseStatusRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
