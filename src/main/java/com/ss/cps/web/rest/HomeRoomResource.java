package com.ss.cps.web.rest;

import com.ss.cps.domain.HomeRoom;
import com.ss.cps.repository.HomeRoomRepository;
import com.ss.cps.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.ss.cps.domain.HomeRoom}.
 */
@RestController
@RequestMapping("/api")
public class HomeRoomResource {

    private final Logger log = LoggerFactory.getLogger(HomeRoomResource.class);

    private static final String ENTITY_NAME = "homeRoom";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final HomeRoomRepository homeRoomRepository;

    public HomeRoomResource(HomeRoomRepository homeRoomRepository) {
        this.homeRoomRepository = homeRoomRepository;
    }

    /**
     * {@code POST  /home-rooms} : Create a new homeRoom.
     *
     * @param homeRoom the homeRoom to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new homeRoom, or with status {@code 400 (Bad Request)} if the homeRoom has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/home-rooms")
    public ResponseEntity<HomeRoom> createHomeRoom(@RequestBody HomeRoom homeRoom) throws URISyntaxException {
        log.debug("REST request to save HomeRoom : {}", homeRoom);
        if (homeRoom.getId() != null) {
            throw new BadRequestAlertException("A new homeRoom cannot already have an ID", ENTITY_NAME, "idexists");
        }
        HomeRoom result = homeRoomRepository.save(homeRoom);
        return ResponseEntity.created(new URI("/api/home-rooms/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /home-rooms} : Updates an existing homeRoom.
     *
     * @param homeRoom the homeRoom to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated homeRoom,
     * or with status {@code 400 (Bad Request)} if the homeRoom is not valid,
     * or with status {@code 500 (Internal Server Error)} if the homeRoom couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/home-rooms")
    public ResponseEntity<HomeRoom> updateHomeRoom(@RequestBody HomeRoom homeRoom) throws URISyntaxException {
        log.debug("REST request to update HomeRoom : {}", homeRoom);
        if (homeRoom.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        HomeRoom result = homeRoomRepository.save(homeRoom);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, homeRoom.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /home-rooms} : get all the homeRooms.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of homeRooms in body.
     */
    @GetMapping("/home-rooms")
    public List<HomeRoom> getAllHomeRooms() {
        log.debug("REST request to get all HomeRooms");
        return homeRoomRepository.findAll();
    }

    /**
     * {@code GET  /home-rooms/:id} : get the "id" homeRoom.
     *
     * @param id the id of the homeRoom to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the homeRoom, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/home-rooms/{id}")
    public ResponseEntity<HomeRoom> getHomeRoom(@PathVariable Long id) {
        log.debug("REST request to get HomeRoom : {}", id);
        Optional<HomeRoom> homeRoom = homeRoomRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(homeRoom);
    }

    /**
     * {@code DELETE  /home-rooms/:id} : delete the "id" homeRoom.
     *
     * @param id the id of the homeRoom to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/home-rooms/{id}")
    public ResponseEntity<Void> deleteHomeRoom(@PathVariable Long id) {
        log.debug("REST request to delete HomeRoom : {}", id);
        homeRoomRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
