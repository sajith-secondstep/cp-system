package com.ss.cps.web.rest;

import com.ss.cps.domain.CpCase;
import com.ss.cps.repository.CpCaseRepository;
import com.ss.cps.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.ss.cps.domain.CpCase}.
 */
@RestController
@RequestMapping("/api")
public class CpCaseResource {

    private final Logger log = LoggerFactory.getLogger(CpCaseResource.class);

    private static final String ENTITY_NAME = "cpCase";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CpCaseRepository cpCaseRepository;

    public CpCaseResource(CpCaseRepository cpCaseRepository) {
        this.cpCaseRepository = cpCaseRepository;
    }

    /**
     * {@code POST  /cp-cases} : Create a new cpCase.
     *
     * @param cpCase the cpCase to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new cpCase, or with status {@code 400 (Bad Request)} if the cpCase has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/cp-cases")
    public ResponseEntity<CpCase> createCpCase(@RequestBody CpCase cpCase) throws URISyntaxException {
        log.debug("REST request to save CpCase : {}", cpCase);
        if (cpCase.getId() != null) {
            throw new BadRequestAlertException("A new cpCase cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CpCase result = cpCaseRepository.save(cpCase);
        return ResponseEntity.created(new URI("/api/cp-cases/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /cp-cases} : Updates an existing cpCase.
     *
     * @param cpCase the cpCase to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated cpCase,
     * or with status {@code 400 (Bad Request)} if the cpCase is not valid,
     * or with status {@code 500 (Internal Server Error)} if the cpCase couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/cp-cases")
    public ResponseEntity<CpCase> updateCpCase(@RequestBody CpCase cpCase) throws URISyntaxException {
        log.debug("REST request to update CpCase : {}", cpCase);
        if (cpCase.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CpCase result = cpCaseRepository.save(cpCase);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, cpCase.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /cp-cases} : get all the cpCases.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of cpCases in body.
     */
    @GetMapping("/cp-cases")
    public List<CpCase> getAllCpCases() {
        log.debug("REST request to get all CpCases");
        return cpCaseRepository.findAll();
    }

    /**
     * {@code GET  /cp-cases/:id} : get the "id" cpCase.
     *
     * @param id the id of the cpCase to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the cpCase, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/cp-cases/{id}")
    public ResponseEntity<CpCase> getCpCase(@PathVariable Long id) {
        log.debug("REST request to get CpCase : {}", id);
        Optional<CpCase> cpCase = cpCaseRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(cpCase);
    }

    /**
     * {@code DELETE  /cp-cases/:id} : delete the "id" cpCase.
     *
     * @param id the id of the cpCase to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/cp-cases/{id}")
    public ResponseEntity<Void> deleteCpCase(@PathVariable Long id) {
        log.debug("REST request to delete CpCase : {}", id);
        cpCaseRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
