package com.ss.cps.config;

/**
 * Application constants.
 */
public final class Constants {

    // Regex for acceptable logins
    public static final String LOGIN_REGEX = "^[_.@A-Za-z0-9-]*$";
    
    public static final String SYSTEM_ACCOUNT = "system";
    public static final String DEFAULT_LANGUAGE = "en";
    public static final String ANONYMOUS_USER = "anonymoususer";

	public static final String STUDENT_NAME = "STUDENT_NAME";

	public static final String SCHOOL_NAME = "SCHOOL_NAME";

    private Constants() {
    }
}
