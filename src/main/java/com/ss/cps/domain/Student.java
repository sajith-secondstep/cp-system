package com.ss.cps.domain;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A Student.
 */
@Entity
@Table(name = "student")
public class Student implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "number")
    private String number;

    @Column(name = "civility")
    private String civility;

    @Column(name = "id_number")
    private String idNumber;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "second_name")
    private String secondName;

    @Column(name = "third_name")
    private String thirdName;

    @Column(name = "fourth_name")
    private String fourthName;

    @Column(name = "family_name")
    private String familyName;

    @Column(name = "birth_name")
    private String birthName;

    @Column(name = "birth_date")
    private LocalDate birthDate;

    @Column(name = "birth_city")
    private String birthCity;

    @Column(name = "birth_country")
    private String birthCountry;

    @Column(name = "nationality")
    private String nationality;

    @Column(name = "english_first_name")
    private String englishFirstName;

    @Column(name = "english_second_name")
    private String englishSecondName;

    @Column(name = "english_third_name")
    private String englishThirdName;

    @Column(name = "english_fourth_name")
    private String englishFourthName;

    @Column(name = "english_family_name")
    private String englishFamilyName;

    @Column(name = "english_birth_city")
    private String englishBirthCity;

    @Column(name = "gender")
    private Integer gender;

    @Column(name = "marital_status")
    private Integer maritalStatus;

    @Column(name = "photo_id")
    private Integer photoId;

    @Column(name = "religion")
    private Integer religion;

    @Column(name = "religion_sector")
    private Integer religionSector;

    @Column(name = "phone_list_id")
    private Integer phoneListId;

    @Column(name = "address_list_id")
    private Integer addressListId;

    @Column(name = "grade")
    private String grade;

    @Column(name = "home_room")
    private String homeRoom;

    @Column(name = "school_id")
    private Integer schoolId;

    @Column(name = "school_name")
    private String schoolName;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public Student number(String number) {
        this.number = number;
        return this;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCivility() {
        return civility;
    }

    public Student civility(String civility) {
        this.civility = civility;
        return this;
    }

    public void setCivility(String civility) {
        this.civility = civility;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public Student idNumber(String idNumber) {
        this.idNumber = idNumber;
        return this;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public Student firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public Student secondName(String secondName) {
        this.secondName = secondName;
        return this;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getThirdName() {
        return thirdName;
    }

    public Student thirdName(String thirdName) {
        this.thirdName = thirdName;
        return this;
    }

    public void setThirdName(String thirdName) {
        this.thirdName = thirdName;
    }

    public String getFourthName() {
        return fourthName;
    }

    public Student fourthName(String fourthName) {
        this.fourthName = fourthName;
        return this;
    }

    public void setFourthName(String fourthName) {
        this.fourthName = fourthName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public Student familyName(String familyName) {
        this.familyName = familyName;
        return this;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getBirthName() {
        return birthName;
    }

    public Student birthName(String birthName) {
        this.birthName = birthName;
        return this;
    }

    public void setBirthName(String birthName) {
        this.birthName = birthName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public Student birthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getBirthCity() {
        return birthCity;
    }

    public Student birthCity(String birthCity) {
        this.birthCity = birthCity;
        return this;
    }

    public void setBirthCity(String birthCity) {
        this.birthCity = birthCity;
    }

    public String getBirthCountry() {
        return birthCountry;
    }

    public Student birthCountry(String birthCountry) {
        this.birthCountry = birthCountry;
        return this;
    }

    public void setBirthCountry(String birthCountry) {
        this.birthCountry = birthCountry;
    }

    public String getNationality() {
        return nationality;
    }

    public Student nationality(String nationality) {
        this.nationality = nationality;
        return this;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getEnglishFirstName() {
        return englishFirstName;
    }

    public Student englishFirstName(String englishFirstName) {
        this.englishFirstName = englishFirstName;
        return this;
    }

    public void setEnglishFirstName(String englishFirstName) {
        this.englishFirstName = englishFirstName;
    }

    public String getEnglishSecondName() {
        return englishSecondName;
    }

    public Student englishSecondName(String englishSecondName) {
        this.englishSecondName = englishSecondName;
        return this;
    }

    public void setEnglishSecondName(String englishSecondName) {
        this.englishSecondName = englishSecondName;
    }

    public String getEnglishThirdName() {
        return englishThirdName;
    }

    public Student englishThirdName(String englishThirdName) {
        this.englishThirdName = englishThirdName;
        return this;
    }

    public void setEnglishThirdName(String englishThirdName) {
        this.englishThirdName = englishThirdName;
    }

    public String getEnglishFourthName() {
        return englishFourthName;
    }

    public Student englishFourthName(String englishFourthName) {
        this.englishFourthName = englishFourthName;
        return this;
    }

    public void setEnglishFourthName(String englishFourthName) {
        this.englishFourthName = englishFourthName;
    }

    public String getEnglishFamilyName() {
        return englishFamilyName;
    }

    public Student englishFamilyName(String englishFamilyName) {
        this.englishFamilyName = englishFamilyName;
        return this;
    }

    public void setEnglishFamilyName(String englishFamilyName) {
        this.englishFamilyName = englishFamilyName;
    }

    public String getEnglishBirthCity() {
        return englishBirthCity;
    }

    public Student englishBirthCity(String englishBirthCity) {
        this.englishBirthCity = englishBirthCity;
        return this;
    }

    public void setEnglishBirthCity(String englishBirthCity) {
        this.englishBirthCity = englishBirthCity;
    }

    public Integer getGender() {
        return gender;
    }

    public Student gender(Integer gender) {
        this.gender = gender;
        return this;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Integer getMaritalStatus() {
        return maritalStatus;
    }

    public Student maritalStatus(Integer maritalStatus) {
        this.maritalStatus = maritalStatus;
        return this;
    }

    public void setMaritalStatus(Integer maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public Integer getPhotoId() {
        return photoId;
    }

    public Student photoId(Integer photoId) {
        this.photoId = photoId;
        return this;
    }

    public void setPhotoId(Integer photoId) {
        this.photoId = photoId;
    }

    public Integer getReligion() {
        return religion;
    }

    public Student religion(Integer religion) {
        this.religion = religion;
        return this;
    }

    public void setReligion(Integer religion) {
        this.religion = religion;
    }

    public Integer getReligionSector() {
        return religionSector;
    }

    public Student religionSector(Integer religionSector) {
        this.religionSector = religionSector;
        return this;
    }

    public void setReligionSector(Integer religionSector) {
        this.religionSector = religionSector;
    }

    public Integer getPhoneListId() {
        return phoneListId;
    }

    public Student phoneListId(Integer phoneListId) {
        this.phoneListId = phoneListId;
        return this;
    }

    public void setPhoneListId(Integer phoneListId) {
        this.phoneListId = phoneListId;
    }

    public Integer getAddressListId() {
        return addressListId;
    }

    public Student addressListId(Integer addressListId) {
        this.addressListId = addressListId;
        return this;
    }

    public void setAddressListId(Integer addressListId) {
        this.addressListId = addressListId;
    }

    public String getGrade() {
        return grade;
    }

    public Student grade(String grade) {
        this.grade = grade;
        return this;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getHomeRoom() {
        return homeRoom;
    }

    public Student homeRoom(String homeRoom) {
        this.homeRoom = homeRoom;
        return this;
    }

    public void setHomeRoom(String homeRoom) {
        this.homeRoom = homeRoom;
    }

    public Integer getSchoolId() {
        return schoolId;
    }

    public Student schoolId(Integer schoolId) {
        this.schoolId = schoolId;
        return this;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public Student schoolName(String schoolName) {
        this.schoolName = schoolName;
        return this;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Student)) {
            return false;
        }
        return id != null && id.equals(((Student) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Student{" +
            "id=" + getId() +
            ", number='" + getNumber() + "'" +
            ", civility='" + getCivility() + "'" +
            ", idNumber='" + getIdNumber() + "'" +
            ", firstName='" + getFirstName() + "'" +
            ", secondName='" + getSecondName() + "'" +
            ", thirdName='" + getThirdName() + "'" +
            ", fourthName='" + getFourthName() + "'" +
            ", familyName='" + getFamilyName() + "'" +
            ", birthName='" + getBirthName() + "'" +
            ", birthDate='" + getBirthDate() + "'" +
            ", birthCity='" + getBirthCity() + "'" +
            ", birthCountry='" + getBirthCountry() + "'" +
            ", nationality='" + getNationality() + "'" +
            ", englishFirstName='" + getEnglishFirstName() + "'" +
            ", englishSecondName='" + getEnglishSecondName() + "'" +
            ", englishThirdName='" + getEnglishThirdName() + "'" +
            ", englishFourthName='" + getEnglishFourthName() + "'" +
            ", englishFamilyName='" + getEnglishFamilyName() + "'" +
            ", englishBirthCity='" + getEnglishBirthCity() + "'" +
            ", gender=" + getGender() +
            ", maritalStatus=" + getMaritalStatus() +
            ", photoId=" + getPhotoId() +
            ", religion=" + getReligion() +
            ", religionSector=" + getReligionSector() +
            ", phoneListId=" + getPhoneListId() +
            ", addressListId=" + getAddressListId() +
            ", grade='" + getGrade() + "'" +
            ", homeRoom='" + getHomeRoom() + "'" +
            ", schoolId=" + getSchoolId() +
            ", schoolName='" + getSchoolName() + "'" +
            "}";
    }
}
