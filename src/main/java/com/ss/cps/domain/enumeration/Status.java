package com.ss.cps.domain.enumeration;

/**
 * The Status enumeration.
 */
public enum Status {
    ACTIVE, INACTIVE, SUSPENDED, CREATED
}
