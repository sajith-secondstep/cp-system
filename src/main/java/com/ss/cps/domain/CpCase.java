package com.ss.cps.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A CpCase.
 */
@Entity
@Table(name = "cp_case")
public class CpCase implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "school_name")
    private String schoolName;

    @Column(name = "school_id")
    private Integer schoolId;

    @Column(name = "student_name")
    private String studentName;

    @Column(name = "student_number")
    private String studentNumber;

    @Column(name = "gender")
    private Integer gender;

    @Column(name = "academic_year")
    private String academicYear;

    @Column(name = "note")
    private String note;

    @Column(name = "reporter_name")
    private String reporterName;

    @Column(name = "cluster")
    private String cluster;

    @Column(name = "email")
    private String email;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "incident_text")
    private String incidentText;

    @Column(name = "incident_type_text")
    private String incidentTypeText;

    @Column(name = "grade_text")
    private String gradeText;

    @Column(name = "home_room_text")
    private String homeRoomText;

    @Column(name = "relationship_text")
    private String relationshipText;

    @Column(name = "officer")
    private String officer;

    @Column(name = "incident_counter")
    private Integer incidentCounter;

    @ManyToOne
    @JsonIgnoreProperties("cpCases")
    private Incident incident;

    @ManyToOne
    @JsonIgnoreProperties("cpCases")
    private IncidentType incidentType;

    @ManyToOne
    @JsonIgnoreProperties("cpCases")
    private Grade grade;

    @ManyToOne
    @JsonIgnoreProperties("cpCases")
    private HomeRoom homeRoom;

    @ManyToOne
    @JsonIgnoreProperties("cpCases")
    private Relationship relationship;

    @ManyToOne
    @JsonIgnoreProperties("cpCases")
    private CaseStatus caseStatus;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public CpCase schoolName(String schoolName) {
        this.schoolName = schoolName;
        return this;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public Integer getSchoolId() {
        return schoolId;
    }

    public CpCase schoolId(Integer schoolId) {
        this.schoolId = schoolId;
        return this;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    public String getStudentName() {
        return studentName;
    }

    public CpCase studentName(String studentName) {
        this.studentName = studentName;
        return this;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentNumber() {
        return studentNumber;
    }

    public CpCase studentNumber(String studentNumber) {
        this.studentNumber = studentNumber;
        return this;
    }

    public void setStudentNumber(String studentNumber) {
        this.studentNumber = studentNumber;
    }

    public Integer getGender() {
        return gender;
    }

    public CpCase gender(Integer gender) {
        this.gender = gender;
        return this;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getAcademicYear() {
        return academicYear;
    }

    public CpCase academicYear(String academicYear) {
        this.academicYear = academicYear;
        return this;
    }

    public void setAcademicYear(String academicYear) {
        this.academicYear = academicYear;
    }

    public String getNote() {
        return note;
    }

    public CpCase note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getReporterName() {
        return reporterName;
    }

    public CpCase reporterName(String reporterName) {
        this.reporterName = reporterName;
        return this;
    }

    public void setReporterName(String reporterName) {
        this.reporterName = reporterName;
    }

    public String getCluster() {
        return cluster;
    }

    public CpCase cluster(String cluster) {
        this.cluster = cluster;
        return this;
    }

    public void setCluster(String cluster) {
        this.cluster = cluster;
    }

    public String getEmail() {
        return email;
    }

    public CpCase email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public CpCase phoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getIncidentText() {
        return incidentText;
    }

    public CpCase incidentText(String incidentText) {
        this.incidentText = incidentText;
        return this;
    }

    public void setIncidentText(String incidentText) {
        this.incidentText = incidentText;
    }

    public String getIncidentTypeText() {
        return incidentTypeText;
    }

    public CpCase incidentTypeText(String incidentTypeText) {
        this.incidentTypeText = incidentTypeText;
        return this;
    }

    public void setIncidentTypeText(String incidentTypeText) {
        this.incidentTypeText = incidentTypeText;
    }

    public String getGradeText() {
        return gradeText;
    }

    public CpCase gradeText(String gradeText) {
        this.gradeText = gradeText;
        return this;
    }

    public void setGradeText(String gradeText) {
        this.gradeText = gradeText;
    }

    public String getHomeRoomText() {
        return homeRoomText;
    }

    public CpCase homeRoomText(String homeRoomText) {
        this.homeRoomText = homeRoomText;
        return this;
    }

    public void setHomeRoomText(String homeRoomText) {
        this.homeRoomText = homeRoomText;
    }

    public String getRelationshipText() {
        return relationshipText;
    }

    public CpCase relationshipText(String relationshipText) {
        this.relationshipText = relationshipText;
        return this;
    }

    public void setRelationshipText(String relationshipText) {
        this.relationshipText = relationshipText;
    }

    public String getOfficer() {
        return officer;
    }

    public CpCase officer(String officer) {
        this.officer = officer;
        return this;
    }

    public void setOfficer(String officer) {
        this.officer = officer;
    }

    public Integer getIncidentCounter() {
        return incidentCounter;
    }

    public CpCase incidentCounter(Integer incidentCounter) {
        this.incidentCounter = incidentCounter;
        return this;
    }

    public void setIncidentCounter(Integer incidentCounter) {
        this.incidentCounter = incidentCounter;
    }

    public Incident getIncident() {
        return incident;
    }

    public CpCase incident(Incident incident) {
        this.incident = incident;
        return this;
    }

    public void setIncident(Incident incident) {
        this.incident = incident;
    }

    public IncidentType getIncidentType() {
        return incidentType;
    }

    public CpCase incidentType(IncidentType incidentType) {
        this.incidentType = incidentType;
        return this;
    }

    public void setIncidentType(IncidentType incidentType) {
        this.incidentType = incidentType;
    }

    public Grade getGrade() {
        return grade;
    }

    public CpCase grade(Grade grade) {
        this.grade = grade;
        return this;
    }

    public void setGrade(Grade grade) {
        this.grade = grade;
    }

    public HomeRoom getHomeRoom() {
        return homeRoom;
    }

    public CpCase homeRoom(HomeRoom homeRoom) {
        this.homeRoom = homeRoom;
        return this;
    }

    public void setHomeRoom(HomeRoom homeRoom) {
        this.homeRoom = homeRoom;
    }

    public Relationship getRelationship() {
        return relationship;
    }

    public CpCase relationship(Relationship relationship) {
        this.relationship = relationship;
        return this;
    }

    public void setRelationship(Relationship relationship) {
        this.relationship = relationship;
    }

    public CaseStatus getCaseStatus() {
        return caseStatus;
    }

    public CpCase caseStatus(CaseStatus caseStatus) {
        this.caseStatus = caseStatus;
        return this;
    }

    public void setCaseStatus(CaseStatus caseStatus) {
        this.caseStatus = caseStatus;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CpCase)) {
            return false;
        }
        return id != null && id.equals(((CpCase) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "CpCase{" +
            "id=" + getId() +
            ", schoolName='" + getSchoolName() + "'" +
            ", schoolId=" + getSchoolId() +
            ", studentName='" + getStudentName() + "'" +
            ", studentNumber='" + getStudentNumber() + "'" +
            ", gender=" + getGender() +
            ", academicYear='" + getAcademicYear() + "'" +
            ", note='" + getNote() + "'" +
            ", reporterName='" + getReporterName() + "'" +
            ", cluster='" + getCluster() + "'" +
            ", email='" + getEmail() + "'" +
            ", phoneNumber='" + getPhoneNumber() + "'" +
            ", incidentText='" + getIncidentText() + "'" +
            ", incidentTypeText='" + getIncidentTypeText() + "'" +
            ", gradeText='" + getGradeText() + "'" +
            ", homeRoomText='" + getHomeRoomText() + "'" +
            ", relationshipText='" + getRelationshipText() + "'" +
            ", officer='" + getOfficer() + "'" +
            ", incidentCounter=" + getIncidentCounter() +
            "}";
    }
}
