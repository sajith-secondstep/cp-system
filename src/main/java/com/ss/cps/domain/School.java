package com.ss.cps.domain;

import javax.persistence.*;

import java.io.Serializable;

import com.ss.cps.domain.enumeration.Status;

/**
 * A School.
 */
@Entity
@Table(name = "school")
public class School implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "short_name")
    private String shortName;

    @Column(name = "code")
    private String code;

    @Column(name = "type")
    private Integer type;

    @Enumerated(EnumType.STRING)
    @Column(name = "note")
    private Status note;

    @Column(name = "english_name")
    private String englishName;

    @Column(name = "manager_id")
    private Integer managerId;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public School name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public School shortName(String shortName) {
        this.shortName = shortName;
        return this;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getCode() {
        return code;
    }

    public School code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getType() {
        return type;
    }

    public School type(Integer type) {
        this.type = type;
        return this;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Status getNote() {
        return note;
    }

    public School note(Status note) {
        this.note = note;
        return this;
    }

    public void setNote(Status note) {
        this.note = note;
    }

    public String getEnglishName() {
        return englishName;
    }

    public School englishName(String englishName) {
        this.englishName = englishName;
        return this;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public Integer getManagerId() {
        return managerId;
    }

    public School managerId(Integer managerId) {
        this.managerId = managerId;
        return this;
    }

    public void setManagerId(Integer managerId) {
        this.managerId = managerId;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof School)) {
            return false;
        }
        return id != null && id.equals(((School) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "School{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", shortName='" + getShortName() + "'" +
            ", code='" + getCode() + "'" +
            ", type=" + getType() +
            ", note='" + getNote() + "'" +
            ", englishName='" + getEnglishName() + "'" +
            ", managerId=" + getManagerId() +
            "}";
    }
}
