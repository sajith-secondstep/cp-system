package com.ss.cps.repository;

import com.ss.cps.domain.HomeRoom;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the HomeRoom entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HomeRoomRepository extends JpaRepository<HomeRoom, Long> {

}
