package com.ss.cps.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ss.cps.domain.School;


/**
 * Spring Data  repository for the School entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SchoolRepository extends JpaRepository<School, Long> {
	@Query(value = "select * from school where name like %?1%", nativeQuery = true)
	List<School> findAllBySchoolName(String schoolName);

}
