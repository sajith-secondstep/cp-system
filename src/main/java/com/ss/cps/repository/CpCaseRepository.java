package com.ss.cps.repository;

import com.ss.cps.domain.CpCase;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the CpCase entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CpCaseRepository extends JpaRepository<CpCase, Long> {

}
