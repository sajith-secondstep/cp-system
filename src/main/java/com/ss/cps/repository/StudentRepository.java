package com.ss.cps.repository;

import com.ss.cps.domain.Student;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Student entity.
 */
@SuppressWarnings("unused")
@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {

	@Query(value = "select * from student where first_name like %?1%", nativeQuery = true)
	List<Student> findAllByStudentName(String studentName);

}
