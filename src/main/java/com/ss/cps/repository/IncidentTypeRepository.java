package com.ss.cps.repository;

import com.ss.cps.domain.IncidentType;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the IncidentType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IncidentTypeRepository extends JpaRepository<IncidentType, Long> {

}
