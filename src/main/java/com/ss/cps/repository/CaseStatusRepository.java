package com.ss.cps.repository;

import com.ss.cps.domain.CaseStatus;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the CaseStatus entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CaseStatusRepository extends JpaRepository<CaseStatus, Long> {

}
