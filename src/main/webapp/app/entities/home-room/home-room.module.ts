import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { CpsSharedModule } from 'app/shared';
import {
  HomeRoomComponent,
  HomeRoomDetailComponent,
  HomeRoomUpdateComponent,
  HomeRoomDeletePopupComponent,
  HomeRoomDeleteDialogComponent,
  homeRoomRoute,
  homeRoomPopupRoute
} from './';

const ENTITY_STATES = [...homeRoomRoute, ...homeRoomPopupRoute];

@NgModule({
  imports: [CpsSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    HomeRoomComponent,
    HomeRoomDetailComponent,
    HomeRoomUpdateComponent,
    HomeRoomDeleteDialogComponent,
    HomeRoomDeletePopupComponent
  ],
  entryComponents: [HomeRoomComponent, HomeRoomUpdateComponent, HomeRoomDeleteDialogComponent, HomeRoomDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CpsHomeRoomModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
