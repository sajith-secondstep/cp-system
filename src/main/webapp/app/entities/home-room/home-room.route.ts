import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { HomeRoom } from 'app/shared/model/home-room.model';
import { HomeRoomService } from './home-room.service';
import { HomeRoomComponent } from './home-room.component';
import { HomeRoomDetailComponent } from './home-room-detail.component';
import { HomeRoomUpdateComponent } from './home-room-update.component';
import { HomeRoomDeletePopupComponent } from './home-room-delete-dialog.component';
import { IHomeRoom } from 'app/shared/model/home-room.model';

@Injectable({ providedIn: 'root' })
export class HomeRoomResolve implements Resolve<IHomeRoom> {
  constructor(private service: HomeRoomService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IHomeRoom> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<HomeRoom>) => response.ok),
        map((homeRoom: HttpResponse<HomeRoom>) => homeRoom.body)
      );
    }
    return of(new HomeRoom());
  }
}

export const homeRoomRoute: Routes = [
  {
    path: '',
    component: HomeRoomComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cpsApp.homeRoom.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: HomeRoomDetailComponent,
    resolve: {
      homeRoom: HomeRoomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cpsApp.homeRoom.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: HomeRoomUpdateComponent,
    resolve: {
      homeRoom: HomeRoomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cpsApp.homeRoom.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: HomeRoomUpdateComponent,
    resolve: {
      homeRoom: HomeRoomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cpsApp.homeRoom.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const homeRoomPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: HomeRoomDeletePopupComponent,
    resolve: {
      homeRoom: HomeRoomResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cpsApp.homeRoom.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
