import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IHomeRoom } from 'app/shared/model/home-room.model';
import { HomeRoomService } from './home-room.service';

@Component({
  selector: 'jhi-home-room-delete-dialog',
  templateUrl: './home-room-delete-dialog.component.html'
})
export class HomeRoomDeleteDialogComponent {
  homeRoom: IHomeRoom;

  constructor(protected homeRoomService: HomeRoomService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.homeRoomService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'homeRoomListModification',
        content: 'Deleted an homeRoom'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-home-room-delete-popup',
  template: ''
})
export class HomeRoomDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ homeRoom }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(HomeRoomDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.homeRoom = homeRoom;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/home-room', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/home-room', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
