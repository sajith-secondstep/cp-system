import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IHomeRoom } from 'app/shared/model/home-room.model';

type EntityResponseType = HttpResponse<IHomeRoom>;
type EntityArrayResponseType = HttpResponse<IHomeRoom[]>;

@Injectable({ providedIn: 'root' })
export class HomeRoomService {
  public resourceUrl = SERVER_API_URL + 'api/home-rooms';

  constructor(protected http: HttpClient) {}

  create(homeRoom: IHomeRoom): Observable<EntityResponseType> {
    return this.http.post<IHomeRoom>(this.resourceUrl, homeRoom, { observe: 'response' });
  }

  update(homeRoom: IHomeRoom): Observable<EntityResponseType> {
    return this.http.put<IHomeRoom>(this.resourceUrl, homeRoom, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IHomeRoom>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IHomeRoom[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
