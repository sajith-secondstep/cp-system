import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IHomeRoom, HomeRoom } from 'app/shared/model/home-room.model';
import { HomeRoomService } from './home-room.service';

@Component({
  selector: 'jhi-home-room-update',
  templateUrl: './home-room-update.component.html'
})
export class HomeRoomUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    name: [],
    code: []
  });

  constructor(protected homeRoomService: HomeRoomService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ homeRoom }) => {
      this.updateForm(homeRoom);
    });
  }

  updateForm(homeRoom: IHomeRoom) {
    this.editForm.patchValue({
      id: homeRoom.id,
      name: homeRoom.name,
      code: homeRoom.code
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const homeRoom = this.createFromForm();
    if (homeRoom.id !== undefined) {
      this.subscribeToSaveResponse(this.homeRoomService.update(homeRoom));
    } else {
      this.subscribeToSaveResponse(this.homeRoomService.create(homeRoom));
    }
  }

  private createFromForm(): IHomeRoom {
    return {
      ...new HomeRoom(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      code: this.editForm.get(['code']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IHomeRoom>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
