import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IHomeRoom } from 'app/shared/model/home-room.model';
import { AccountService } from 'app/core';
import { HomeRoomService } from './home-room.service';

@Component({
  selector: 'jhi-home-room',
  templateUrl: './home-room.component.html'
})
export class HomeRoomComponent implements OnInit, OnDestroy {
  homeRooms: IHomeRoom[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected homeRoomService: HomeRoomService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.homeRoomService
      .query()
      .pipe(
        filter((res: HttpResponse<IHomeRoom[]>) => res.ok),
        map((res: HttpResponse<IHomeRoom[]>) => res.body)
      )
      .subscribe(
        (res: IHomeRoom[]) => {
          this.homeRooms = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInHomeRooms();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IHomeRoom) {
    return item.id;
  }

  registerChangeInHomeRooms() {
    this.eventSubscriber = this.eventManager.subscribe('homeRoomListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
