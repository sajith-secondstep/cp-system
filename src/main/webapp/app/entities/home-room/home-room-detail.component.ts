import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IHomeRoom } from 'app/shared/model/home-room.model';

@Component({
  selector: 'jhi-home-room-detail',
  templateUrl: './home-room-detail.component.html'
})
export class HomeRoomDetailComponent implements OnInit {
  homeRoom: IHomeRoom;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ homeRoom }) => {
      this.homeRoom = homeRoom;
    });
  }

  previousState() {
    window.history.back();
  }
}
