import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IGrade } from 'app/shared/model/grade.model';
import { AccountService } from 'app/core';
import { GradeService } from './grade.service';

@Component({
  selector: 'jhi-grade',
  templateUrl: './grade.component.html'
})
export class GradeComponent implements OnInit, OnDestroy {
  grades: IGrade[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected gradeService: GradeService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.gradeService
      .query()
      .pipe(
        filter((res: HttpResponse<IGrade[]>) => res.ok),
        map((res: HttpResponse<IGrade[]>) => res.body)
      )
      .subscribe(
        (res: IGrade[]) => {
          this.grades = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInGrades();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IGrade) {
    return item.id;
  }

  registerChangeInGrades() {
    this.eventSubscriber = this.eventManager.subscribe('gradeListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
