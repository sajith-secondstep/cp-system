import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IIncidentType } from 'app/shared/model/incident-type.model';
import { AccountService } from 'app/core';
import { IncidentTypeService } from './incident-type.service';

@Component({
  selector: 'jhi-incident-type',
  templateUrl: './incident-type.component.html'
})
export class IncidentTypeComponent implements OnInit, OnDestroy {
  incidentTypes: IIncidentType[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected incidentTypeService: IncidentTypeService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.incidentTypeService
      .query()
      .pipe(
        filter((res: HttpResponse<IIncidentType[]>) => res.ok),
        map((res: HttpResponse<IIncidentType[]>) => res.body)
      )
      .subscribe(
        (res: IIncidentType[]) => {
          this.incidentTypes = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInIncidentTypes();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IIncidentType) {
    return item.id;
  }

  registerChangeInIncidentTypes() {
    this.eventSubscriber = this.eventManager.subscribe('incidentTypeListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
