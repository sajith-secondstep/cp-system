import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { CpsSharedModule } from 'app/shared';
import {
  IncidentTypeComponent,
  IncidentTypeDetailComponent,
  IncidentTypeUpdateComponent,
  IncidentTypeDeletePopupComponent,
  IncidentTypeDeleteDialogComponent,
  incidentTypeRoute,
  incidentTypePopupRoute
} from './';

const ENTITY_STATES = [...incidentTypeRoute, ...incidentTypePopupRoute];

@NgModule({
  imports: [CpsSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    IncidentTypeComponent,
    IncidentTypeDetailComponent,
    IncidentTypeUpdateComponent,
    IncidentTypeDeleteDialogComponent,
    IncidentTypeDeletePopupComponent
  ],
  entryComponents: [
    IncidentTypeComponent,
    IncidentTypeUpdateComponent,
    IncidentTypeDeleteDialogComponent,
    IncidentTypeDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CpsIncidentTypeModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
