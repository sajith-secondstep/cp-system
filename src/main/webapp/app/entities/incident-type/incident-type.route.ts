import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { IncidentType } from 'app/shared/model/incident-type.model';
import { IncidentTypeService } from './incident-type.service';
import { IncidentTypeComponent } from './incident-type.component';
import { IncidentTypeDetailComponent } from './incident-type-detail.component';
import { IncidentTypeUpdateComponent } from './incident-type-update.component';
import { IncidentTypeDeletePopupComponent } from './incident-type-delete-dialog.component';
import { IIncidentType } from 'app/shared/model/incident-type.model';

@Injectable({ providedIn: 'root' })
export class IncidentTypeResolve implements Resolve<IIncidentType> {
  constructor(private service: IncidentTypeService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IIncidentType> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<IncidentType>) => response.ok),
        map((incidentType: HttpResponse<IncidentType>) => incidentType.body)
      );
    }
    return of(new IncidentType());
  }
}

export const incidentTypeRoute: Routes = [
  {
    path: '',
    component: IncidentTypeComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cpsApp.incidentType.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: IncidentTypeDetailComponent,
    resolve: {
      incidentType: IncidentTypeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cpsApp.incidentType.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: IncidentTypeUpdateComponent,
    resolve: {
      incidentType: IncidentTypeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cpsApp.incidentType.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: IncidentTypeUpdateComponent,
    resolve: {
      incidentType: IncidentTypeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cpsApp.incidentType.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const incidentTypePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: IncidentTypeDeletePopupComponent,
    resolve: {
      incidentType: IncidentTypeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cpsApp.incidentType.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
