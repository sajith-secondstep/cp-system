import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IIncidentType } from 'app/shared/model/incident-type.model';
import { IncidentTypeService } from './incident-type.service';

@Component({
  selector: 'jhi-incident-type-delete-dialog',
  templateUrl: './incident-type-delete-dialog.component.html'
})
export class IncidentTypeDeleteDialogComponent {
  incidentType: IIncidentType;

  constructor(
    protected incidentTypeService: IncidentTypeService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.incidentTypeService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'incidentTypeListModification',
        content: 'Deleted an incidentType'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-incident-type-delete-popup',
  template: ''
})
export class IncidentTypeDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ incidentType }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(IncidentTypeDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.incidentType = incidentType;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/incident-type', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/incident-type', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
