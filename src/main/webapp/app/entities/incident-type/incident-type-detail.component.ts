import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IIncidentType } from 'app/shared/model/incident-type.model';

@Component({
  selector: 'jhi-incident-type-detail',
  templateUrl: './incident-type-detail.component.html'
})
export class IncidentTypeDetailComponent implements OnInit {
  incidentType: IIncidentType;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ incidentType }) => {
      this.incidentType = incidentType;
    });
  }

  previousState() {
    window.history.back();
  }
}
