import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IIncidentType, IncidentType } from 'app/shared/model/incident-type.model';
import { IncidentTypeService } from './incident-type.service';

@Component({
  selector: 'jhi-incident-type-update',
  templateUrl: './incident-type-update.component.html'
})
export class IncidentTypeUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    name: [],
    code: []
  });

  constructor(protected incidentTypeService: IncidentTypeService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ incidentType }) => {
      this.updateForm(incidentType);
    });
  }

  updateForm(incidentType: IIncidentType) {
    this.editForm.patchValue({
      id: incidentType.id,
      name: incidentType.name,
      code: incidentType.code
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const incidentType = this.createFromForm();
    if (incidentType.id !== undefined) {
      this.subscribeToSaveResponse(this.incidentTypeService.update(incidentType));
    } else {
      this.subscribeToSaveResponse(this.incidentTypeService.create(incidentType));
    }
  }

  private createFromForm(): IIncidentType {
    return {
      ...new IncidentType(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      code: this.editForm.get(['code']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IIncidentType>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
