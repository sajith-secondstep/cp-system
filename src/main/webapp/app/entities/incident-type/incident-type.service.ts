import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IIncidentType } from 'app/shared/model/incident-type.model';

type EntityResponseType = HttpResponse<IIncidentType>;
type EntityArrayResponseType = HttpResponse<IIncidentType[]>;

@Injectable({ providedIn: 'root' })
export class IncidentTypeService {
  public resourceUrl = SERVER_API_URL + 'api/incident-types';

  constructor(protected http: HttpClient) {}

  create(incidentType: IIncidentType): Observable<EntityResponseType> {
    return this.http.post<IIncidentType>(this.resourceUrl, incidentType, { observe: 'response' });
  }

  update(incidentType: IIncidentType): Observable<EntityResponseType> {
    return this.http.put<IIncidentType>(this.resourceUrl, incidentType, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IIncidentType>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IIncidentType[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
