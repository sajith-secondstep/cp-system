import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'country',
        loadChildren: () => import('./country/country.module').then(m => m.CpsCountryModule)
      },
      {
        path: 'state',
        loadChildren: () => import('./state/state.module').then(m => m.CpsStateModule)
      },
      {
        path: 'school',
        loadChildren: () => import('./school/school.module').then(m => m.CpsSchoolModule)
      },
      {
        path: 'student',
        loadChildren: () => import('./student/student.module').then(m => m.CpsStudentModule)
      },
      {
        path: 'incident',
        loadChildren: () => import('./incident/incident.module').then(m => m.CpsIncidentModule)
      },
      {
        path: 'incident-type',
        loadChildren: () => import('./incident-type/incident-type.module').then(m => m.CpsIncidentTypeModule)
      },
      {
        path: 'grade',
        loadChildren: () => import('./grade/grade.module').then(m => m.CpsGradeModule)
      },
      {
        path: 'home-room',
        loadChildren: () => import('./home-room/home-room.module').then(m => m.CpsHomeRoomModule)
      },
      {
        path: 'relationship',
        loadChildren: () => import('./relationship/relationship.module').then(m => m.CpsRelationshipModule)
      },
      {
        path: 'cp-case',
        loadChildren: () => import('./cp-case/cp-case.module').then(m => m.CpsCpCaseModule)
      },
      {
        path: 'case-status',
        loadChildren: () => import('./case-status/case-status.module').then(m => m.CpsCaseStatusModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ],
  declarations: [],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CpsEntityModule {}
