import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ICaseStatus, CaseStatus } from 'app/shared/model/case-status.model';
import { CaseStatusService } from './case-status.service';

@Component({
  selector: 'jhi-case-status-update',
  templateUrl: './case-status-update.component.html'
})
export class CaseStatusUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    name: [],
    code: []
  });

  constructor(protected caseStatusService: CaseStatusService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ caseStatus }) => {
      this.updateForm(caseStatus);
    });
  }

  updateForm(caseStatus: ICaseStatus) {
    this.editForm.patchValue({
      id: caseStatus.id,
      name: caseStatus.name,
      code: caseStatus.code
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const caseStatus = this.createFromForm();
    if (caseStatus.id !== undefined) {
      this.subscribeToSaveResponse(this.caseStatusService.update(caseStatus));
    } else {
      this.subscribeToSaveResponse(this.caseStatusService.create(caseStatus));
    }
  }

  private createFromForm(): ICaseStatus {
    return {
      ...new CaseStatus(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      code: this.editForm.get(['code']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICaseStatus>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
