import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { CpsSharedModule } from 'app/shared';
import {
  CaseStatusComponent,
  CaseStatusDetailComponent,
  CaseStatusUpdateComponent,
  CaseStatusDeletePopupComponent,
  CaseStatusDeleteDialogComponent,
  caseStatusRoute,
  caseStatusPopupRoute
} from './';

const ENTITY_STATES = [...caseStatusRoute, ...caseStatusPopupRoute];

@NgModule({
  imports: [CpsSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    CaseStatusComponent,
    CaseStatusDetailComponent,
    CaseStatusUpdateComponent,
    CaseStatusDeleteDialogComponent,
    CaseStatusDeletePopupComponent
  ],
  entryComponents: [CaseStatusComponent, CaseStatusUpdateComponent, CaseStatusDeleteDialogComponent, CaseStatusDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CpsCaseStatusModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
