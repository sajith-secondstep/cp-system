import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICaseStatus } from 'app/shared/model/case-status.model';
import { CaseStatusService } from './case-status.service';

@Component({
  selector: 'jhi-case-status-delete-dialog',
  templateUrl: './case-status-delete-dialog.component.html'
})
export class CaseStatusDeleteDialogComponent {
  caseStatus: ICaseStatus;

  constructor(
    protected caseStatusService: CaseStatusService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.caseStatusService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'caseStatusListModification',
        content: 'Deleted an caseStatus'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-case-status-delete-popup',
  template: ''
})
export class CaseStatusDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ caseStatus }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(CaseStatusDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.caseStatus = caseStatus;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/case-status', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/case-status', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
