import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ICaseStatus } from 'app/shared/model/case-status.model';

type EntityResponseType = HttpResponse<ICaseStatus>;
type EntityArrayResponseType = HttpResponse<ICaseStatus[]>;

@Injectable({ providedIn: 'root' })
export class CaseStatusService {
  public resourceUrl = SERVER_API_URL + 'api/case-statuses';

  constructor(protected http: HttpClient) {}

  create(caseStatus: ICaseStatus): Observable<EntityResponseType> {
    return this.http.post<ICaseStatus>(this.resourceUrl, caseStatus, { observe: 'response' });
  }

  update(caseStatus: ICaseStatus): Observable<EntityResponseType> {
    return this.http.put<ICaseStatus>(this.resourceUrl, caseStatus, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICaseStatus>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICaseStatus[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
