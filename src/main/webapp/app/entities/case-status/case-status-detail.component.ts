import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICaseStatus } from 'app/shared/model/case-status.model';

@Component({
  selector: 'jhi-case-status-detail',
  templateUrl: './case-status-detail.component.html'
})
export class CaseStatusDetailComponent implements OnInit {
  caseStatus: ICaseStatus;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ caseStatus }) => {
      this.caseStatus = caseStatus;
    });
  }

  previousState() {
    window.history.back();
  }
}
