import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { CaseStatus } from 'app/shared/model/case-status.model';
import { CaseStatusService } from './case-status.service';
import { CaseStatusComponent } from './case-status.component';
import { CaseStatusDetailComponent } from './case-status-detail.component';
import { CaseStatusUpdateComponent } from './case-status-update.component';
import { CaseStatusDeletePopupComponent } from './case-status-delete-dialog.component';
import { ICaseStatus } from 'app/shared/model/case-status.model';

@Injectable({ providedIn: 'root' })
export class CaseStatusResolve implements Resolve<ICaseStatus> {
  constructor(private service: CaseStatusService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICaseStatus> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<CaseStatus>) => response.ok),
        map((caseStatus: HttpResponse<CaseStatus>) => caseStatus.body)
      );
    }
    return of(new CaseStatus());
  }
}

export const caseStatusRoute: Routes = [
  {
    path: '',
    component: CaseStatusComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cpsApp.caseStatus.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: CaseStatusDetailComponent,
    resolve: {
      caseStatus: CaseStatusResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cpsApp.caseStatus.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: CaseStatusUpdateComponent,
    resolve: {
      caseStatus: CaseStatusResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cpsApp.caseStatus.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: CaseStatusUpdateComponent,
    resolve: {
      caseStatus: CaseStatusResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cpsApp.caseStatus.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const caseStatusPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: CaseStatusDeletePopupComponent,
    resolve: {
      caseStatus: CaseStatusResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cpsApp.caseStatus.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
