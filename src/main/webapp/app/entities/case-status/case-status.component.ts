import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ICaseStatus } from 'app/shared/model/case-status.model';
import { AccountService } from 'app/core';
import { CaseStatusService } from './case-status.service';

@Component({
  selector: 'jhi-case-status',
  templateUrl: './case-status.component.html'
})
export class CaseStatusComponent implements OnInit, OnDestroy {
  caseStatuses: ICaseStatus[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected caseStatusService: CaseStatusService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.caseStatusService
      .query()
      .pipe(
        filter((res: HttpResponse<ICaseStatus[]>) => res.ok),
        map((res: HttpResponse<ICaseStatus[]>) => res.body)
      )
      .subscribe(
        (res: ICaseStatus[]) => {
          this.caseStatuses = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInCaseStatuses();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: ICaseStatus) {
    return item.id;
  }

  registerChangeInCaseStatuses() {
    this.eventSubscriber = this.eventManager.subscribe('caseStatusListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
