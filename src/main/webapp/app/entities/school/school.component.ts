import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, Injector } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ISchool } from 'app/shared/model/school.model';
import { AccountService } from 'app/core';
import { SchoolService } from './school.service';
import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'jhi-school',
  templateUrl: './school.component.html'
})
export class SchoolComponent implements OnInit, OnDestroy {
  schools: ISchool[];
  currentAccount: any;
  eventSubscriber: Subscription;
  schoolName: string;

  @Input()
  isModal: boolean = false;
  activeModal: NgbActiveModal;

  @Output() passEntry: EventEmitter<any> = new EventEmitter();

  constructor(
    protected schoolService: SchoolService,
    private injector: Injector,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  loadAll() {
    this.schoolService
      .query()
      .pipe(
        filter((res: HttpResponse<ISchool[]>) => res.ok),
        map((res: HttpResponse<ISchool[]>) => res.body)
      )
      .subscribe(
        (res: ISchool[]) => {
          this.schools = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    if (this.isModal) {
      this.activeModal = <NgbActiveModal>this.injector.get(NgbActiveModal);
    }
    //this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInSchools();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: ISchool) {
    return item.id;
  }

  registerChangeInSchools() {
    this.eventSubscriber = this.eventManager.subscribe('schoolListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  serachSchool() {
    var req = {
      SCHOOL_NAME: this.schoolName
    };

    this.schoolService
      .query(req)
      .pipe(
        filter((res: HttpResponse<ISchool[]>) => res.ok),
        map((res: HttpResponse<ISchool[]>) => res.body)
      )
      .subscribe(
        (res: ISchool[]) => {
          this.schools = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  handleUserClick(school) {
    // Create Book logic
    console.log('!!!!!!!!!!!!  11111111111 >>>> ' + school);
    if (this.isModal) {
      this.passEntry.emit(school);
      if (this.isModal) {
        this.clear();
      }
    }
  }
}
