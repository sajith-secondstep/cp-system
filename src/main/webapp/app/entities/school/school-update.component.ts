import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ISchool, School } from 'app/shared/model/school.model';
import { SchoolService } from './school.service';

@Component({
  selector: 'jhi-school-update',
  templateUrl: './school-update.component.html'
})
export class SchoolUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    name: [],
    shortName: [],
    code: [],
    type: [],
    note: [],
    englishName: [],
    managerId: []
  });

  constructor(protected schoolService: SchoolService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ school }) => {
      this.updateForm(school);
    });
  }

  updateForm(school: ISchool) {
    this.editForm.patchValue({
      id: school.id,
      name: school.name,
      shortName: school.shortName,
      code: school.code,
      type: school.type,
      note: school.note,
      englishName: school.englishName,
      managerId: school.managerId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const school = this.createFromForm();
    if (school.id !== undefined) {
      this.subscribeToSaveResponse(this.schoolService.update(school));
    } else {
      this.subscribeToSaveResponse(this.schoolService.create(school));
    }
  }

  private createFromForm(): ISchool {
    return {
      ...new School(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      shortName: this.editForm.get(['shortName']).value,
      code: this.editForm.get(['code']).value,
      type: this.editForm.get(['type']).value,
      note: this.editForm.get(['note']).value,
      englishName: this.editForm.get(['englishName']).value,
      managerId: this.editForm.get(['managerId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISchool>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
