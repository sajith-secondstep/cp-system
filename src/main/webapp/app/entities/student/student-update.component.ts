import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { IStudent, Student } from 'app/shared/model/student.model';
import { StudentService } from './student.service';

@Component({
  selector: 'jhi-student-update',
  templateUrl: './student-update.component.html'
})
export class StudentUpdateComponent implements OnInit {
  isSaving: boolean;
  birthDateDp: any;

  editForm = this.fb.group({
    id: [],
    civility: [],
    idNumber: [],
    firstName: [],
    secondName: [],
    thirdName: [],
    fourthName: [],
    familyName: [],
    birthName: [],
    birthDate: [],
    birthCity: [],
    birthCountry: [],
    nationality: [],
    englishFirstName: [],
    englishSecondName: [],
    englishThirdName: [],
    englishFourthName: [],
    englishFamilyName: [],
    englishBirthCity: [],
    gender: [],
    maritalStatus: [],
    photoId: [],
    religion: [],
    religionSector: [],
    phoneListId: [],
    addressListId: [],
    grade: [],
    homeRoom: [],
    schoolId: [],
    schoolName: []
  });

  constructor(protected studentService: StudentService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ student }) => {
      this.updateForm(student);
    });
  }

  updateForm(student: IStudent) {
    this.editForm.patchValue({
      id: student.id,
      civility: student.civility,
      idNumber: student.idNumber,
      firstName: student.firstName,
      secondName: student.secondName,
      thirdName: student.thirdName,
      fourthName: student.fourthName,
      familyName: student.familyName,
      birthName: student.birthName,
      birthDate: student.birthDate,
      birthCity: student.birthCity,
      birthCountry: student.birthCountry,
      nationality: student.nationality,
      englishFirstName: student.englishFirstName,
      englishSecondName: student.englishSecondName,
      englishThirdName: student.englishThirdName,
      englishFourthName: student.englishFourthName,
      englishFamilyName: student.englishFamilyName,
      englishBirthCity: student.englishBirthCity,
      gender: student.gender,
      maritalStatus: student.maritalStatus,
      photoId: student.photoId,
      religion: student.religion,
      religionSector: student.religionSector,
      phoneListId: student.phoneListId,
      addressListId: student.addressListId,
      grade: student.grade,
      homeRoom: student.homeRoom,
      schoolId: student.schoolId,
      schoolName: student.schoolName
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const student = this.createFromForm();
    if (student.id !== undefined) {
      this.subscribeToSaveResponse(this.studentService.update(student));
    } else {
      this.subscribeToSaveResponse(this.studentService.create(student));
    }
  }

  private createFromForm(): IStudent {
    return {
      ...new Student(),
      id: this.editForm.get(['id']).value,
      civility: this.editForm.get(['civility']).value,
      idNumber: this.editForm.get(['idNumber']).value,
      firstName: this.editForm.get(['firstName']).value,
      secondName: this.editForm.get(['secondName']).value,
      thirdName: this.editForm.get(['thirdName']).value,
      fourthName: this.editForm.get(['fourthName']).value,
      familyName: this.editForm.get(['familyName']).value,
      birthName: this.editForm.get(['birthName']).value,
      birthDate: this.editForm.get(['birthDate']).value,
      birthCity: this.editForm.get(['birthCity']).value,
      birthCountry: this.editForm.get(['birthCountry']).value,
      nationality: this.editForm.get(['nationality']).value,
      englishFirstName: this.editForm.get(['englishFirstName']).value,
      englishSecondName: this.editForm.get(['englishSecondName']).value,
      englishThirdName: this.editForm.get(['englishThirdName']).value,
      englishFourthName: this.editForm.get(['englishFourthName']).value,
      englishFamilyName: this.editForm.get(['englishFamilyName']).value,
      englishBirthCity: this.editForm.get(['englishBirthCity']).value,
      gender: this.editForm.get(['gender']).value,
      maritalStatus: this.editForm.get(['maritalStatus']).value,
      photoId: this.editForm.get(['photoId']).value,
      religion: this.editForm.get(['religion']).value,
      religionSector: this.editForm.get(['religionSector']).value,
      phoneListId: this.editForm.get(['phoneListId']).value,
      addressListId: this.editForm.get(['addressListId']).value,
      grade: this.editForm.get(['grade']).value,
      homeRoom: this.editForm.get(['homeRoom']).value,
      schoolId: this.editForm.get(['schoolId']).value,
      schoolName: this.editForm.get(['schoolName']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IStudent>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
