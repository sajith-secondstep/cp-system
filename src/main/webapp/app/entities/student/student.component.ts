import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, Injector } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IStudent } from 'app/shared/model/student.model';
import { AccountService } from 'app/core';
import { StudentService } from './student.service';
import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'jhi-student',
  templateUrl: './student.component.html'
})
export class StudentComponent implements OnInit, OnDestroy {
  students: IStudent[];
  currentAccount: any;
  eventSubscriber: Subscription;
  studentName: string;

  @Input()
  isModal: boolean = false;
  activeModal: NgbActiveModal;

  @Output() passEntry: EventEmitter<any> = new EventEmitter();

  constructor(
    protected studentService: StudentService,
    private injector: Injector, //public activeModal: NgbActiveModal,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  loadAll() {
    this.studentService
      .query()
      .pipe(
        filter((res: HttpResponse<IStudent[]>) => res.ok),
        map((res: HttpResponse<IStudent[]>) => res.body)
      )
      .subscribe(
        (res: IStudent[]) => {
          this.students = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    console.log(' @@@@@  StudentComponent : isModal -> ' + this.isModal);
    if (this.isModal) {
      this.activeModal = <NgbActiveModal>this.injector.get(NgbActiveModal);
    }
    // this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInStudents();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IStudent) {
    return item.id;
  }

  registerChangeInStudents() {
    this.eventSubscriber = this.eventManager.subscribe('studentListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  serachStudent() {
    // Create Book logic
    console.log('!!!!!!!!!!!!  11111111111 >>>> ' + this.studentName);

    var req = {
      STUDENT_NAME: this.studentName
    };

    this.studentService
      .query(req)
      .pipe(
        filter((res: HttpResponse<IStudent[]>) => res.ok),
        map((res: HttpResponse<IStudent[]>) => res.body)
      )
      .subscribe(
        (res: IStudent[]) => {
          this.students = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );

    /*
    this.createButonClicked.emit(); 
    if(this.bcMeetingId != null && this.bcMeetingId > 0){
      console.log("!!!!!!!!!!!!  11111111111 >>>> "+ this.bcMeetingId);
      this.router.navigate(['/bc-agenda/new',{'bcMeetingId':this.bcMeetingId}]);
    }else{
      this.router.navigate(['/bc-agenda/new']);
    }
    */
  }

  handleUserClick(student) {
    // Create Book logic
    console.log('!!!!!!!!!!!!  11111111111 >>>> ' + student);
    if (this.isModal) {
      this.passEntry.emit(student);
      if (this.isModal) {
        this.clear();
      }
    }
  }
}
