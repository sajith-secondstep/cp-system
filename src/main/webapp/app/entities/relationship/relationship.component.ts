import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IRelationship } from 'app/shared/model/relationship.model';
import { AccountService } from 'app/core';
import { RelationshipService } from './relationship.service';

@Component({
  selector: 'jhi-relationship',
  templateUrl: './relationship.component.html'
})
export class RelationshipComponent implements OnInit, OnDestroy {
  relationships: IRelationship[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected relationshipService: RelationshipService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.relationshipService
      .query()
      .pipe(
        filter((res: HttpResponse<IRelationship[]>) => res.ok),
        map((res: HttpResponse<IRelationship[]>) => res.body)
      )
      .subscribe(
        (res: IRelationship[]) => {
          this.relationships = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInRelationships();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IRelationship) {
    return item.id;
  }

  registerChangeInRelationships() {
    this.eventSubscriber = this.eventManager.subscribe('relationshipListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
