import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { CpsSharedModule } from 'app/shared';
import {
  RelationshipComponent,
  RelationshipDetailComponent,
  RelationshipUpdateComponent,
  RelationshipDeletePopupComponent,
  RelationshipDeleteDialogComponent,
  relationshipRoute,
  relationshipPopupRoute
} from './';

const ENTITY_STATES = [...relationshipRoute, ...relationshipPopupRoute];

@NgModule({
  imports: [CpsSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    RelationshipComponent,
    RelationshipDetailComponent,
    RelationshipUpdateComponent,
    RelationshipDeleteDialogComponent,
    RelationshipDeletePopupComponent
  ],
  entryComponents: [
    RelationshipComponent,
    RelationshipUpdateComponent,
    RelationshipDeleteDialogComponent,
    RelationshipDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CpsRelationshipModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
