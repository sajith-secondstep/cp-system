import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IRelationship, Relationship } from 'app/shared/model/relationship.model';
import { RelationshipService } from './relationship.service';

@Component({
  selector: 'jhi-relationship-update',
  templateUrl: './relationship-update.component.html'
})
export class RelationshipUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    name: [],
    code: []
  });

  constructor(protected relationshipService: RelationshipService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ relationship }) => {
      this.updateForm(relationship);
    });
  }

  updateForm(relationship: IRelationship) {
    this.editForm.patchValue({
      id: relationship.id,
      name: relationship.name,
      code: relationship.code
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const relationship = this.createFromForm();
    if (relationship.id !== undefined) {
      this.subscribeToSaveResponse(this.relationshipService.update(relationship));
    } else {
      this.subscribeToSaveResponse(this.relationshipService.create(relationship));
    }
  }

  private createFromForm(): IRelationship {
    return {
      ...new Relationship(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      code: this.editForm.get(['code']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRelationship>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
