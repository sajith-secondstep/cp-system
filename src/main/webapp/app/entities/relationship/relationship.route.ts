import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Relationship } from 'app/shared/model/relationship.model';
import { RelationshipService } from './relationship.service';
import { RelationshipComponent } from './relationship.component';
import { RelationshipDetailComponent } from './relationship-detail.component';
import { RelationshipUpdateComponent } from './relationship-update.component';
import { RelationshipDeletePopupComponent } from './relationship-delete-dialog.component';
import { IRelationship } from 'app/shared/model/relationship.model';

@Injectable({ providedIn: 'root' })
export class RelationshipResolve implements Resolve<IRelationship> {
  constructor(private service: RelationshipService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IRelationship> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Relationship>) => response.ok),
        map((relationship: HttpResponse<Relationship>) => relationship.body)
      );
    }
    return of(new Relationship());
  }
}

export const relationshipRoute: Routes = [
  {
    path: '',
    component: RelationshipComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cpsApp.relationship.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: RelationshipDetailComponent,
    resolve: {
      relationship: RelationshipResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cpsApp.relationship.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: RelationshipUpdateComponent,
    resolve: {
      relationship: RelationshipResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cpsApp.relationship.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: RelationshipUpdateComponent,
    resolve: {
      relationship: RelationshipResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cpsApp.relationship.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const relationshipPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: RelationshipDeletePopupComponent,
    resolve: {
      relationship: RelationshipResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cpsApp.relationship.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
