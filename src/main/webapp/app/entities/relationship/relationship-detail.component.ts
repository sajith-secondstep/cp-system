import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRelationship } from 'app/shared/model/relationship.model';

@Component({
  selector: 'jhi-relationship-detail',
  templateUrl: './relationship-detail.component.html'
})
export class RelationshipDetailComponent implements OnInit {
  relationship: IRelationship;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ relationship }) => {
      this.relationship = relationship;
    });
  }

  previousState() {
    window.history.back();
  }
}
