export * from './relationship.service';
export * from './relationship-update.component';
export * from './relationship-delete-dialog.component';
export * from './relationship-detail.component';
export * from './relationship.component';
export * from './relationship.route';
