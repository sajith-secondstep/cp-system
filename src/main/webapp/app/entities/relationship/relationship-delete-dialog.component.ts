import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IRelationship } from 'app/shared/model/relationship.model';
import { RelationshipService } from './relationship.service';

@Component({
  selector: 'jhi-relationship-delete-dialog',
  templateUrl: './relationship-delete-dialog.component.html'
})
export class RelationshipDeleteDialogComponent {
  relationship: IRelationship;

  constructor(
    protected relationshipService: RelationshipService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.relationshipService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'relationshipListModification',
        content: 'Deleted an relationship'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-relationship-delete-popup',
  template: ''
})
export class RelationshipDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ relationship }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(RelationshipDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.relationship = relationship;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/relationship', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/relationship', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
