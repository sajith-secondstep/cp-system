import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IState, State } from 'app/shared/model/state.model';
import { StateService } from './state.service';

@Component({
  selector: 'jhi-state-update',
  templateUrl: './state-update.component.html'
})
export class StateUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    name: [],
    code: []
  });

  constructor(protected stateService: StateService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ state }) => {
      this.updateForm(state);
    });
  }

  updateForm(state: IState) {
    this.editForm.patchValue({
      id: state.id,
      name: state.name,
      code: state.code
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const state = this.createFromForm();
    if (state.id !== undefined) {
      this.subscribeToSaveResponse(this.stateService.update(state));
    } else {
      this.subscribeToSaveResponse(this.stateService.create(state));
    }
  }

  private createFromForm(): IState {
    return {
      ...new State(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      code: this.editForm.get(['code']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IState>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
