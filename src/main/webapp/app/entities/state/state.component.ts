import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IState } from 'app/shared/model/state.model';
import { AccountService } from 'app/core';
import { StateService } from './state.service';

@Component({
  selector: 'jhi-state',
  templateUrl: './state.component.html'
})
export class StateComponent implements OnInit, OnDestroy {
  states: IState[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected stateService: StateService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.stateService
      .query()
      .pipe(
        filter((res: HttpResponse<IState[]>) => res.ok),
        map((res: HttpResponse<IState[]>) => res.body)
      )
      .subscribe(
        (res: IState[]) => {
          this.states = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInStates();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IState) {
    return item.id;
  }

  registerChangeInStates() {
    this.eventSubscriber = this.eventManager.subscribe('stateListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
