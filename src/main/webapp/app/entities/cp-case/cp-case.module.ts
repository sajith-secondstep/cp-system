import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { CpsSharedModule } from 'app/shared';
import { CpsStudentModule } from 'app/entities/student/student.module';
import { CpsSchoolModule } from 'app/entities/school/school.module';
import {
  CpCaseComponent,
  CpCaseDetailComponent,
  CpCaseUpdateComponent,
  CpCaseDeletePopupComponent,
  CpCaseDeleteDialogComponent,
  cpCaseRoute,
  cpCasePopupRoute
} from './';

const ENTITY_STATES = [...cpCaseRoute, ...cpCasePopupRoute];

@NgModule({
  imports: [CpsSharedModule, CpsStudentModule, CpsSchoolModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [CpCaseComponent, CpCaseDetailComponent, CpCaseUpdateComponent, CpCaseDeleteDialogComponent, CpCaseDeletePopupComponent],
  entryComponents: [CpCaseComponent, CpCaseUpdateComponent, CpCaseDeleteDialogComponent, CpCaseDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CpsCpCaseModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
