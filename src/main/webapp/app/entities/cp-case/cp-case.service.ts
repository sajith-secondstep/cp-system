import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ICpCase } from 'app/shared/model/cp-case.model';

type EntityResponseType = HttpResponse<ICpCase>;
type EntityArrayResponseType = HttpResponse<ICpCase[]>;

@Injectable({ providedIn: 'root' })
export class CpCaseService {
  public resourceUrl = SERVER_API_URL + 'api/cp-cases';

  constructor(protected http: HttpClient) {}

  create(cpCase: ICpCase): Observable<EntityResponseType> {
    return this.http.post<ICpCase>(this.resourceUrl, cpCase, { observe: 'response' });
  }

  update(cpCase: ICpCase): Observable<EntityResponseType> {
    return this.http.put<ICpCase>(this.resourceUrl, cpCase, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICpCase>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICpCase[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
