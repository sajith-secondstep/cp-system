import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { CpCase } from 'app/shared/model/cp-case.model';
import { CpCaseService } from './cp-case.service';
import { CpCaseComponent } from './cp-case.component';
import { CpCaseDetailComponent } from './cp-case-detail.component';
import { CpCaseUpdateComponent } from './cp-case-update.component';
import { CpCaseDeletePopupComponent } from './cp-case-delete-dialog.component';
import { ICpCase } from 'app/shared/model/cp-case.model';

@Injectable({ providedIn: 'root' })
export class CpCaseResolve implements Resolve<ICpCase> {
  constructor(private service: CpCaseService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICpCase> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<CpCase>) => response.ok),
        map((cpCase: HttpResponse<CpCase>) => cpCase.body)
      );
    }
    return of(new CpCase());
  }
}

export const cpCaseRoute: Routes = [
  {
    path: '',
    component: CpCaseComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cpsApp.cpCase.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: CpCaseDetailComponent,
    resolve: {
      cpCase: CpCaseResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cpsApp.cpCase.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: CpCaseUpdateComponent,
    resolve: {
      cpCase: CpCaseResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cpsApp.cpCase.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: CpCaseUpdateComponent,
    resolve: {
      cpCase: CpCaseResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cpsApp.cpCase.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const cpCasePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: CpCaseDeletePopupComponent,
    resolve: {
      cpCase: CpCaseResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cpsApp.cpCase.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
