import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ICpCase, CpCase } from 'app/shared/model/cp-case.model';
import { CpCaseService } from './cp-case.service';
import { IIncident } from 'app/shared/model/incident.model';
import { IncidentService } from 'app/entities/incident';
import { IIncidentType } from 'app/shared/model/incident-type.model';
import { IncidentTypeService } from 'app/entities/incident-type';
import { IGrade } from 'app/shared/model/grade.model';
import { GradeService } from 'app/entities/grade';
import { IHomeRoom } from 'app/shared/model/home-room.model';
import { HomeRoomService } from 'app/entities/home-room';
import { IRelationship } from 'app/shared/model/relationship.model';
import { RelationshipService } from 'app/entities/relationship';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { StudentComponent } from 'app/entities/student/student.component';
import { IStudent, Student } from 'app/shared/model/student.model';

import { SchoolComponent } from 'app/entities/school/school.component';
import { ISchool, School } from 'app/shared/model/school.model';

@Component({
  selector: 'jhi-cp-case-update',
  templateUrl: './cp-case-update.component.html'
})
export class CpCaseUpdateComponent implements OnInit {
  isSaving: boolean;

  incidents: IIncident[];

  incidenttypes: IIncidentType[];

  grades: IGrade[];

  homerooms: IHomeRoom[];

  relationships: IRelationship[];

  protected ngbModalRef: NgbModalRef;
  selectedStudent: Student = null;
  selectedSchool: School = null;

  editForm = this.fb.group({
    id: [],
    schoolName: [],
    schoolId: [],
    studentName: [],
    gender: [],
    note: [],
    reporterName: [],
    cluster: [],
    email: [],
    phoneNumber: [],
    incidentText: [],
    incidentTypeText: [],
    gradeText: [],
    homeRoomText: [],
    relationshipText: [],
    incident: [],
    incidentType: [],
    grade: [],
    homeRoom: [],
    relationship: [],
    officer: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected cpCaseService: CpCaseService,
    protected incidentService: IncidentService,
    protected incidentTypeService: IncidentTypeService,
    protected gradeService: GradeService,
    protected homeRoomService: HomeRoomService,
    protected relationshipService: RelationshipService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    protected modalService: NgbModal,
    protected router: Router
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ cpCase }) => {
      this.updateForm(cpCase);
    });
    this.incidentService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IIncident[]>) => mayBeOk.ok),
        map((response: HttpResponse<IIncident[]>) => response.body)
      )
      .subscribe((res: IIncident[]) => (this.incidents = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.incidentTypeService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IIncidentType[]>) => mayBeOk.ok),
        map((response: HttpResponse<IIncidentType[]>) => response.body)
      )
      .subscribe((res: IIncidentType[]) => (this.incidenttypes = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.gradeService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IGrade[]>) => mayBeOk.ok),
        map((response: HttpResponse<IGrade[]>) => response.body)
      )
      .subscribe((res: IGrade[]) => (this.grades = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.homeRoomService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IHomeRoom[]>) => mayBeOk.ok),
        map((response: HttpResponse<IHomeRoom[]>) => response.body)
      )
      .subscribe((res: IHomeRoom[]) => (this.homerooms = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.relationshipService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IRelationship[]>) => mayBeOk.ok),
        map((response: HttpResponse<IRelationship[]>) => response.body)
      )
      .subscribe((res: IRelationship[]) => (this.relationships = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(cpCase: ICpCase) {
    this.editForm.patchValue({
      id: cpCase.id,
      schoolName: cpCase.schoolName,
      schoolId: cpCase.schoolId,
      studentName: cpCase.studentName,
      gender: cpCase.gender,
      note: cpCase.note,
      reporterName: cpCase.reporterName,
      cluster: cpCase.cluster,
      email: cpCase.email,
      phoneNumber: cpCase.phoneNumber,
      incidentText: cpCase.incidentText,
      incidentTypeText: cpCase.incidentTypeText,
      gradeText: cpCase.gradeText,
      homeRoomText: cpCase.homeRoomText,
      relationshipText: cpCase.relationshipText,
      incident: cpCase.incident,
      incidentType: cpCase.incidentType,
      grade: cpCase.grade,
      homeRoom: cpCase.homeRoom,
      relationship: cpCase.relationship,
      officer: cpCase.officer
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const cpCase = this.createFromForm();
    if (cpCase.id !== undefined) {
      this.subscribeToSaveResponse(this.cpCaseService.update(cpCase));
    } else {
      this.subscribeToSaveResponse(this.cpCaseService.create(cpCase));
    }
  }

  private createFromForm(): ICpCase {
    return {
      ...new CpCase(),
      id: this.editForm.get(['id']).value,
      schoolName: this.editForm.get(['schoolName']).value,
      schoolId: this.editForm.get(['schoolId']).value,
      studentName: this.editForm.get(['studentName']).value,
      gender: this.editForm.get(['gender']).value,
      note: this.editForm.get(['note']).value,
      reporterName: this.editForm.get(['reporterName']).value,
      cluster: this.editForm.get(['cluster']).value,
      email: this.editForm.get(['email']).value,
      phoneNumber: this.editForm.get(['phoneNumber']).value,
      incidentText: this.editForm.get(['incidentText']).value,
      incidentTypeText: this.editForm.get(['incidentTypeText']).value,
      gradeText: this.editForm.get(['gradeText']).value,
      homeRoomText: this.editForm.get(['homeRoomText']).value,
      relationshipText: this.editForm.get(['relationshipText']).value,
      incident: this.editForm.get(['incident']).value,
      incidentType: this.editForm.get(['incidentType']).value,
      grade: this.editForm.get(['grade']).value,
      homeRoom: this.editForm.get(['homeRoom']).value,
      officer: this.editForm.get(['officer']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICpCase>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackIncidentById(index: number, item: IIncident) {
    return item.id;
  }

  trackIncidentTypeById(index: number, item: IIncidentType) {
    return item.id;
  }

  trackGradeById(index: number, item: IGrade) {
    return item.id;
  }

  trackHomeRoomById(index: number, item: IHomeRoom) {
    return item.id;
  }

  trackRelationshipById(index: number, item: IRelationship) {
    return item.id;
  }

  serachStudentPopup() {
    setTimeout(() => {
      this.ngbModalRef = this.modalService.open(StudentComponent as Component, { size: 'lg', backdrop: 'static' });
      this.ngbModalRef.componentInstance.isModal = true;

      this.ngbModalRef.componentInstance.passEntry.subscribe(receivedEntry => {
        console.log(receivedEntry);
        this.editForm.patchValue({ studentName: receivedEntry.firstName });
        this.selectedStudent = receivedEntry;
      });
    }, 0);
  }

  serachSchoolPopup() {
    setTimeout(() => {
      this.ngbModalRef = this.modalService.open(SchoolComponent as Component, { size: 'lg', backdrop: 'static' });
      this.ngbModalRef.componentInstance.isModal = true;

      this.ngbModalRef.componentInstance.passEntry.subscribe(receivedEntry => {
        console.log(receivedEntry);
        this.editForm.patchValue({
          schoolName: receivedEntry.name,
          schoolId: receivedEntry.id
        });
        this.selectedSchool = receivedEntry;
      });
    }, 0);
  }
}
