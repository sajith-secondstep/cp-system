import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICpCase } from 'app/shared/model/cp-case.model';

@Component({
  selector: 'jhi-cp-case-detail',
  templateUrl: './cp-case-detail.component.html'
})
export class CpCaseDetailComponent implements OnInit {
  cpCase: ICpCase;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ cpCase }) => {
      this.cpCase = cpCase;
    });
  }

  previousState() {
    window.history.back();
  }
}
