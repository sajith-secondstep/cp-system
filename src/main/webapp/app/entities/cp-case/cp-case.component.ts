import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ICpCase } from 'app/shared/model/cp-case.model';
import { AccountService } from 'app/core';
import { CpCaseService } from './cp-case.service';

@Component({
  selector: 'jhi-cp-case',
  templateUrl: './cp-case.component.html'
})
export class CpCaseComponent implements OnInit, OnDestroy {
  cpCases: ICpCase[];
  currentAccount: any;
  eventSubscriber: Subscription;
  caseNumber: Number;

  constructor(
    protected cpCaseService: CpCaseService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.cpCaseService
      .query()
      .pipe(
        filter((res: HttpResponse<ICpCase[]>) => res.ok),
        map((res: HttpResponse<ICpCase[]>) => res.body)
      )
      .subscribe(
        (res: ICpCase[]) => {
          this.cpCases = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInCpCases();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: ICpCase) {
    return item.id;
  }

  registerChangeInCpCases() {
    this.eventSubscriber = this.eventManager.subscribe('cpCaseListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
