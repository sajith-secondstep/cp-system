import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICpCase } from 'app/shared/model/cp-case.model';
import { CpCaseService } from './cp-case.service';

@Component({
  selector: 'jhi-cp-case-delete-dialog',
  templateUrl: './cp-case-delete-dialog.component.html'
})
export class CpCaseDeleteDialogComponent {
  cpCase: ICpCase;

  constructor(protected cpCaseService: CpCaseService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.cpCaseService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'cpCaseListModification',
        content: 'Deleted an cpCase'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-cp-case-delete-popup',
  template: ''
})
export class CpCaseDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ cpCase }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(CpCaseDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.cpCase = cpCase;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/cp-case', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/cp-case', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
