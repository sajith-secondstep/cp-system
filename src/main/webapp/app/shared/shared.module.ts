import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CpsSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [CpsSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [CpsSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CpsSharedModule {
  static forRoot() {
    return {
      ngModule: CpsSharedModule
    };
  }
}
