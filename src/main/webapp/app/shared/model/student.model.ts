import { Moment } from 'moment';

export interface IStudent {
  id?: number;
  number?: string;
  civility?: string;
  idNumber?: string;
  firstName?: string;
  secondName?: string;
  thirdName?: string;
  fourthName?: string;
  familyName?: string;
  birthName?: string;
  birthDate?: Moment;
  birthCity?: string;
  birthCountry?: string;
  nationality?: string;
  englishFirstName?: string;
  englishSecondName?: string;
  englishThirdName?: string;
  englishFourthName?: string;
  englishFamilyName?: string;
  englishBirthCity?: string;
  gender?: number;
  maritalStatus?: number;
  photoId?: number;
  religion?: number;
  religionSector?: number;
  phoneListId?: number;
  addressListId?: number;
  grade?: string;
  homeRoom?: string;
  schoolId?: number;
  schoolName?: string;
}

export class Student implements IStudent {
  constructor(
    public id?: number,
    public number?: string,
    public civility?: string,
    public idNumber?: string,
    public firstName?: string,
    public secondName?: string,
    public thirdName?: string,
    public fourthName?: string,
    public familyName?: string,
    public birthName?: string,
    public birthDate?: Moment,
    public birthCity?: string,
    public birthCountry?: string,
    public nationality?: string,
    public englishFirstName?: string,
    public englishSecondName?: string,
    public englishThirdName?: string,
    public englishFourthName?: string,
    public englishFamilyName?: string,
    public englishBirthCity?: string,
    public gender?: number,
    public maritalStatus?: number,
    public photoId?: number,
    public religion?: number,
    public religionSector?: number,
    public phoneListId?: number,
    public addressListId?: number,
    public grade?: string,
    public homeRoom?: string,
    public schoolId?: number,
    public schoolName?: string
  ) {}
}
