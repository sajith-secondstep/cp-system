export interface IIncidentType {
  id?: number;
  name?: string;
  code?: string;
}

export class IncidentType implements IIncidentType {
  constructor(public id?: number, public name?: string, public code?: string) {}
}
