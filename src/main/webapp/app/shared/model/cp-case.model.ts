import { IIncident } from 'app/shared/model/incident.model';
import { IIncidentType } from 'app/shared/model/incident-type.model';
import { IGrade } from 'app/shared/model/grade.model';
import { IHomeRoom } from 'app/shared/model/home-room.model';
import { IRelationship } from 'app/shared/model/relationship.model';
import { ICaseStatus } from 'app/shared/model/case-status.model';

export interface ICpCase {
  id?: number;
  schoolName?: string;
  schoolId?: number;
  studentName?: string;
  studentNumber?: string;
  gender?: number;
  academicYear?: string;
  note?: string;
  reporterName?: string;
  cluster?: string;
  email?: string;
  phoneNumber?: string;
  incidentText?: string;
  incidentTypeText?: string;
  gradeText?: string;
  homeRoomText?: string;
  relationshipText?: string;
  officer?: string;
  incidentCounter?: number;
  incident?: IIncident;
  incidentType?: IIncidentType;
  grade?: IGrade;
  homeRoom?: IHomeRoom;
  relationship?: IRelationship;
  caseStatus?: ICaseStatus;
}

export class CpCase implements ICpCase {
  constructor(
    public id?: number,
    public schoolName?: string,
    public schoolId?: number,
    public studentName?: string,
    public studentNumber?: string,
    public gender?: number,
    public academicYear?: string,
    public note?: string,
    public reporterName?: string,
    public cluster?: string,
    public email?: string,
    public phoneNumber?: string,
    public incidentText?: string,
    public incidentTypeText?: string,
    public gradeText?: string,
    public homeRoomText?: string,
    public relationshipText?: string,
    public officer?: string,
    public incidentCounter?: number,
    public incident?: IIncident,
    public incidentType?: IIncidentType,
    public grade?: IGrade,
    public homeRoom?: IHomeRoom,
    public relationship?: IRelationship,
    public caseStatus?: ICaseStatus
  ) {}
}
