export interface IIncident {
  id?: number;
  name?: string;
  code?: string;
}

export class Incident implements IIncident {
  constructor(public id?: number, public name?: string, public code?: string) {}
}
