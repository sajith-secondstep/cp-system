export interface IState {
  id?: number;
  name?: string;
  code?: string;
}

export class State implements IState {
  constructor(public id?: number, public name?: string, public code?: string) {}
}
