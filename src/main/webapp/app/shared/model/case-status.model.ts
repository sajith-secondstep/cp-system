export interface ICaseStatus {
  id?: number;
  name?: string;
  code?: string;
}

export class CaseStatus implements ICaseStatus {
  constructor(public id?: number, public name?: string, public code?: string) {}
}
