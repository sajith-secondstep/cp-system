export interface IGrade {
  id?: number;
  name?: string;
  code?: string;
}

export class Grade implements IGrade {
  constructor(public id?: number, public name?: string, public code?: string) {}
}
