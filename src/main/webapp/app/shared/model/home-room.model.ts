export interface IHomeRoom {
  id?: number;
  name?: string;
  code?: string;
}

export class HomeRoom implements IHomeRoom {
  constructor(public id?: number, public name?: string, public code?: string) {}
}
