export interface IRelationship {
  id?: number;
  name?: string;
  code?: string;
}

export class Relationship implements IRelationship {
  constructor(public id?: number, public name?: string, public code?: string) {}
}
