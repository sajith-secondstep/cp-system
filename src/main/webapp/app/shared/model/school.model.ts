export const enum Status {
  ACTIVE = 'ACTIVE',
  INACTIVE = 'INACTIVE',
  SUSPENDED = 'SUSPENDED',
  CREATED = 'CREATED'
}

export interface ISchool {
  id?: number;
  name?: string;
  shortName?: string;
  code?: string;
  type?: number;
  note?: Status;
  englishName?: string;
  managerId?: number;
}

export class School implements ISchool {
  constructor(
    public id?: number,
    public name?: string,
    public shortName?: string,
    public code?: string,
    public type?: number,
    public note?: Status,
    public englishName?: string,
    public managerId?: number
  ) {}
}
