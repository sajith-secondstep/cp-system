/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CpsTestModule } from '../../../test.module';
import { HomeRoomDetailComponent } from 'app/entities/home-room/home-room-detail.component';
import { HomeRoom } from 'app/shared/model/home-room.model';

describe('Component Tests', () => {
  describe('HomeRoom Management Detail Component', () => {
    let comp: HomeRoomDetailComponent;
    let fixture: ComponentFixture<HomeRoomDetailComponent>;
    const route = ({ data: of({ homeRoom: new HomeRoom(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CpsTestModule],
        declarations: [HomeRoomDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(HomeRoomDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(HomeRoomDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.homeRoom).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
