/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { CpsTestModule } from '../../../test.module';
import { HomeRoomComponent } from 'app/entities/home-room/home-room.component';
import { HomeRoomService } from 'app/entities/home-room/home-room.service';
import { HomeRoom } from 'app/shared/model/home-room.model';

describe('Component Tests', () => {
  describe('HomeRoom Management Component', () => {
    let comp: HomeRoomComponent;
    let fixture: ComponentFixture<HomeRoomComponent>;
    let service: HomeRoomService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CpsTestModule],
        declarations: [HomeRoomComponent],
        providers: []
      })
        .overrideTemplate(HomeRoomComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(HomeRoomComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(HomeRoomService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new HomeRoom(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.homeRooms[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
