/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { CpsTestModule } from '../../../test.module';
import { HomeRoomDeleteDialogComponent } from 'app/entities/home-room/home-room-delete-dialog.component';
import { HomeRoomService } from 'app/entities/home-room/home-room.service';

describe('Component Tests', () => {
  describe('HomeRoom Management Delete Component', () => {
    let comp: HomeRoomDeleteDialogComponent;
    let fixture: ComponentFixture<HomeRoomDeleteDialogComponent>;
    let service: HomeRoomService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CpsTestModule],
        declarations: [HomeRoomDeleteDialogComponent]
      })
        .overrideTemplate(HomeRoomDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(HomeRoomDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(HomeRoomService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
