/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { CpsTestModule } from '../../../test.module';
import { HomeRoomUpdateComponent } from 'app/entities/home-room/home-room-update.component';
import { HomeRoomService } from 'app/entities/home-room/home-room.service';
import { HomeRoom } from 'app/shared/model/home-room.model';

describe('Component Tests', () => {
  describe('HomeRoom Management Update Component', () => {
    let comp: HomeRoomUpdateComponent;
    let fixture: ComponentFixture<HomeRoomUpdateComponent>;
    let service: HomeRoomService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CpsTestModule],
        declarations: [HomeRoomUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(HomeRoomUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(HomeRoomUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(HomeRoomService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new HomeRoom(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new HomeRoom();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
