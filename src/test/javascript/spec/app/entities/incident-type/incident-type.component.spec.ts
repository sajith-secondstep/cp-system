/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { CpsTestModule } from '../../../test.module';
import { IncidentTypeComponent } from 'app/entities/incident-type/incident-type.component';
import { IncidentTypeService } from 'app/entities/incident-type/incident-type.service';
import { IncidentType } from 'app/shared/model/incident-type.model';

describe('Component Tests', () => {
  describe('IncidentType Management Component', () => {
    let comp: IncidentTypeComponent;
    let fixture: ComponentFixture<IncidentTypeComponent>;
    let service: IncidentTypeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CpsTestModule],
        declarations: [IncidentTypeComponent],
        providers: []
      })
        .overrideTemplate(IncidentTypeComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(IncidentTypeComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(IncidentTypeService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new IncidentType(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.incidentTypes[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
