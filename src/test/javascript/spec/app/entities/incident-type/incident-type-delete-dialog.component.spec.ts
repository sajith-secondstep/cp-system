/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { CpsTestModule } from '../../../test.module';
import { IncidentTypeDeleteDialogComponent } from 'app/entities/incident-type/incident-type-delete-dialog.component';
import { IncidentTypeService } from 'app/entities/incident-type/incident-type.service';

describe('Component Tests', () => {
  describe('IncidentType Management Delete Component', () => {
    let comp: IncidentTypeDeleteDialogComponent;
    let fixture: ComponentFixture<IncidentTypeDeleteDialogComponent>;
    let service: IncidentTypeService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CpsTestModule],
        declarations: [IncidentTypeDeleteDialogComponent]
      })
        .overrideTemplate(IncidentTypeDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(IncidentTypeDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(IncidentTypeService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
