/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CpsTestModule } from '../../../test.module';
import { IncidentTypeDetailComponent } from 'app/entities/incident-type/incident-type-detail.component';
import { IncidentType } from 'app/shared/model/incident-type.model';

describe('Component Tests', () => {
  describe('IncidentType Management Detail Component', () => {
    let comp: IncidentTypeDetailComponent;
    let fixture: ComponentFixture<IncidentTypeDetailComponent>;
    const route = ({ data: of({ incidentType: new IncidentType(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CpsTestModule],
        declarations: [IncidentTypeDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(IncidentTypeDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(IncidentTypeDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.incidentType).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
