/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { CpsTestModule } from '../../../test.module';
import { IncidentTypeUpdateComponent } from 'app/entities/incident-type/incident-type-update.component';
import { IncidentTypeService } from 'app/entities/incident-type/incident-type.service';
import { IncidentType } from 'app/shared/model/incident-type.model';

describe('Component Tests', () => {
  describe('IncidentType Management Update Component', () => {
    let comp: IncidentTypeUpdateComponent;
    let fixture: ComponentFixture<IncidentTypeUpdateComponent>;
    let service: IncidentTypeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CpsTestModule],
        declarations: [IncidentTypeUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(IncidentTypeUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(IncidentTypeUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(IncidentTypeService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new IncidentType(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new IncidentType();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
