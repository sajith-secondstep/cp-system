/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { CpsTestModule } from '../../../test.module';
import { CaseStatusComponent } from 'app/entities/case-status/case-status.component';
import { CaseStatusService } from 'app/entities/case-status/case-status.service';
import { CaseStatus } from 'app/shared/model/case-status.model';

describe('Component Tests', () => {
  describe('CaseStatus Management Component', () => {
    let comp: CaseStatusComponent;
    let fixture: ComponentFixture<CaseStatusComponent>;
    let service: CaseStatusService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CpsTestModule],
        declarations: [CaseStatusComponent],
        providers: []
      })
        .overrideTemplate(CaseStatusComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CaseStatusComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CaseStatusService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new CaseStatus(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.caseStatuses[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
