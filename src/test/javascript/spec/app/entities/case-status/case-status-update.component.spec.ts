/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { CpsTestModule } from '../../../test.module';
import { CaseStatusUpdateComponent } from 'app/entities/case-status/case-status-update.component';
import { CaseStatusService } from 'app/entities/case-status/case-status.service';
import { CaseStatus } from 'app/shared/model/case-status.model';

describe('Component Tests', () => {
  describe('CaseStatus Management Update Component', () => {
    let comp: CaseStatusUpdateComponent;
    let fixture: ComponentFixture<CaseStatusUpdateComponent>;
    let service: CaseStatusService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CpsTestModule],
        declarations: [CaseStatusUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(CaseStatusUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CaseStatusUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CaseStatusService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new CaseStatus(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new CaseStatus();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
