/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CpsTestModule } from '../../../test.module';
import { CaseStatusDetailComponent } from 'app/entities/case-status/case-status-detail.component';
import { CaseStatus } from 'app/shared/model/case-status.model';

describe('Component Tests', () => {
  describe('CaseStatus Management Detail Component', () => {
    let comp: CaseStatusDetailComponent;
    let fixture: ComponentFixture<CaseStatusDetailComponent>;
    const route = ({ data: of({ caseStatus: new CaseStatus(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CpsTestModule],
        declarations: [CaseStatusDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(CaseStatusDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CaseStatusDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.caseStatus).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
