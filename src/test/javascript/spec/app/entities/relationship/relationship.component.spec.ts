/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { CpsTestModule } from '../../../test.module';
import { RelationshipComponent } from 'app/entities/relationship/relationship.component';
import { RelationshipService } from 'app/entities/relationship/relationship.service';
import { Relationship } from 'app/shared/model/relationship.model';

describe('Component Tests', () => {
  describe('Relationship Management Component', () => {
    let comp: RelationshipComponent;
    let fixture: ComponentFixture<RelationshipComponent>;
    let service: RelationshipService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CpsTestModule],
        declarations: [RelationshipComponent],
        providers: []
      })
        .overrideTemplate(RelationshipComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(RelationshipComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(RelationshipService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Relationship(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.relationships[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
