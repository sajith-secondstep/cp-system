/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { CpsTestModule } from '../../../test.module';
import { RelationshipUpdateComponent } from 'app/entities/relationship/relationship-update.component';
import { RelationshipService } from 'app/entities/relationship/relationship.service';
import { Relationship } from 'app/shared/model/relationship.model';

describe('Component Tests', () => {
  describe('Relationship Management Update Component', () => {
    let comp: RelationshipUpdateComponent;
    let fixture: ComponentFixture<RelationshipUpdateComponent>;
    let service: RelationshipService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CpsTestModule],
        declarations: [RelationshipUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(RelationshipUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(RelationshipUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(RelationshipService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Relationship(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Relationship();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
