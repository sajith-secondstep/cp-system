/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CpsTestModule } from '../../../test.module';
import { RelationshipDetailComponent } from 'app/entities/relationship/relationship-detail.component';
import { Relationship } from 'app/shared/model/relationship.model';

describe('Component Tests', () => {
  describe('Relationship Management Detail Component', () => {
    let comp: RelationshipDetailComponent;
    let fixture: ComponentFixture<RelationshipDetailComponent>;
    const route = ({ data: of({ relationship: new Relationship(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CpsTestModule],
        declarations: [RelationshipDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(RelationshipDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(RelationshipDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.relationship).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
