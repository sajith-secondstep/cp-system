/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { CpsTestModule } from '../../../test.module';
import { RelationshipDeleteDialogComponent } from 'app/entities/relationship/relationship-delete-dialog.component';
import { RelationshipService } from 'app/entities/relationship/relationship.service';

describe('Component Tests', () => {
  describe('Relationship Management Delete Component', () => {
    let comp: RelationshipDeleteDialogComponent;
    let fixture: ComponentFixture<RelationshipDeleteDialogComponent>;
    let service: RelationshipService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CpsTestModule],
        declarations: [RelationshipDeleteDialogComponent]
      })
        .overrideTemplate(RelationshipDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(RelationshipDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(RelationshipService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
