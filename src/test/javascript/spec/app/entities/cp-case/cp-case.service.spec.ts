/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import { CpCaseService } from 'app/entities/cp-case/cp-case.service';
import { ICpCase, CpCase } from 'app/shared/model/cp-case.model';

describe('Service Tests', () => {
  describe('CpCase Service', () => {
    let injector: TestBed;
    let service: CpCaseService;
    let httpMock: HttpTestingController;
    let elemDefault: ICpCase;
    let expectedResult;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(CpCaseService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new CpCase(
        0,
        'AAAAAAA',
        0,
        'AAAAAAA',
        'AAAAAAA',
        0,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        0
      );
    });

    describe('Service methods', () => {
      it('should find an element', async () => {
        const returnedFromService = Object.assign({}, elemDefault);
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a CpCase', async () => {
        const returnedFromService = Object.assign(
          {
            id: 0
          },
          elemDefault
        );
        const expected = Object.assign({}, returnedFromService);
        service
          .create(new CpCase(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a CpCase', async () => {
        const returnedFromService = Object.assign(
          {
            schoolName: 'BBBBBB',
            schoolId: 1,
            studentName: 'BBBBBB',
            studentNumber: 'BBBBBB',
            gender: 1,
            academicYear: 'BBBBBB',
            note: 'BBBBBB',
            reporterName: 'BBBBBB',
            cluster: 'BBBBBB',
            email: 'BBBBBB',
            phoneNumber: 'BBBBBB',
            incidentText: 'BBBBBB',
            incidentTypeText: 'BBBBBB',
            gradeText: 'BBBBBB',
            homeRoomText: 'BBBBBB',
            relationshipText: 'BBBBBB',
            officer: 'BBBBBB',
            incidentCounter: 1
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of CpCase', async () => {
        const returnedFromService = Object.assign(
          {
            schoolName: 'BBBBBB',
            schoolId: 1,
            studentName: 'BBBBBB',
            studentNumber: 'BBBBBB',
            gender: 1,
            academicYear: 'BBBBBB',
            note: 'BBBBBB',
            reporterName: 'BBBBBB',
            cluster: 'BBBBBB',
            email: 'BBBBBB',
            phoneNumber: 'BBBBBB',
            incidentText: 'BBBBBB',
            incidentTypeText: 'BBBBBB',
            gradeText: 'BBBBBB',
            homeRoomText: 'BBBBBB',
            relationshipText: 'BBBBBB',
            officer: 'BBBBBB',
            incidentCounter: 1
          },
          elemDefault
        );
        const expected = Object.assign({}, returnedFromService);
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a CpCase', async () => {
        const rxPromise = service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
