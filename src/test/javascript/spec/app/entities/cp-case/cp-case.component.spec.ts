/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { CpsTestModule } from '../../../test.module';
import { CpCaseComponent } from 'app/entities/cp-case/cp-case.component';
import { CpCaseService } from 'app/entities/cp-case/cp-case.service';
import { CpCase } from 'app/shared/model/cp-case.model';

describe('Component Tests', () => {
  describe('CpCase Management Component', () => {
    let comp: CpCaseComponent;
    let fixture: ComponentFixture<CpCaseComponent>;
    let service: CpCaseService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CpsTestModule],
        declarations: [CpCaseComponent],
        providers: []
      })
        .overrideTemplate(CpCaseComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CpCaseComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CpCaseService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new CpCase(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.cpCases[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
