/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { CpsTestModule } from '../../../test.module';
import { CpCaseUpdateComponent } from 'app/entities/cp-case/cp-case-update.component';
import { CpCaseService } from 'app/entities/cp-case/cp-case.service';
import { CpCase } from 'app/shared/model/cp-case.model';

describe('Component Tests', () => {
  describe('CpCase Management Update Component', () => {
    let comp: CpCaseUpdateComponent;
    let fixture: ComponentFixture<CpCaseUpdateComponent>;
    let service: CpCaseService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CpsTestModule],
        declarations: [CpCaseUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(CpCaseUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CpCaseUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CpCaseService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new CpCase(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new CpCase();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
