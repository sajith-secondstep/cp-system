/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { CpsTestModule } from '../../../test.module';
import { CpCaseDeleteDialogComponent } from 'app/entities/cp-case/cp-case-delete-dialog.component';
import { CpCaseService } from 'app/entities/cp-case/cp-case.service';

describe('Component Tests', () => {
  describe('CpCase Management Delete Component', () => {
    let comp: CpCaseDeleteDialogComponent;
    let fixture: ComponentFixture<CpCaseDeleteDialogComponent>;
    let service: CpCaseService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CpsTestModule],
        declarations: [CpCaseDeleteDialogComponent]
      })
        .overrideTemplate(CpCaseDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CpCaseDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CpCaseService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
