/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CpsTestModule } from '../../../test.module';
import { CpCaseDetailComponent } from 'app/entities/cp-case/cp-case-detail.component';
import { CpCase } from 'app/shared/model/cp-case.model';

describe('Component Tests', () => {
  describe('CpCase Management Detail Component', () => {
    let comp: CpCaseDetailComponent;
    let fixture: ComponentFixture<CpCaseDetailComponent>;
    const route = ({ data: of({ cpCase: new CpCase(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CpsTestModule],
        declarations: [CpCaseDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(CpCaseDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CpCaseDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.cpCase).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
