package com.ss.cps.web.rest;

import com.ss.cps.CpsApp;
import com.ss.cps.domain.HomeRoom;
import com.ss.cps.repository.HomeRoomRepository;
import com.ss.cps.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.ss.cps.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link HomeRoomResource} REST controller.
 */
@SpringBootTest(classes = CpsApp.class)
public class HomeRoomResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    @Autowired
    private HomeRoomRepository homeRoomRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restHomeRoomMockMvc;

    private HomeRoom homeRoom;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final HomeRoomResource homeRoomResource = new HomeRoomResource(homeRoomRepository);
        this.restHomeRoomMockMvc = MockMvcBuilders.standaloneSetup(homeRoomResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static HomeRoom createEntity(EntityManager em) {
        HomeRoom homeRoom = new HomeRoom()
            .name(DEFAULT_NAME)
            .code(DEFAULT_CODE);
        return homeRoom;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static HomeRoom createUpdatedEntity(EntityManager em) {
        HomeRoom homeRoom = new HomeRoom()
            .name(UPDATED_NAME)
            .code(UPDATED_CODE);
        return homeRoom;
    }

    @BeforeEach
    public void initTest() {
        homeRoom = createEntity(em);
    }

    @Test
    @Transactional
    public void createHomeRoom() throws Exception {
        int databaseSizeBeforeCreate = homeRoomRepository.findAll().size();

        // Create the HomeRoom
        restHomeRoomMockMvc.perform(post("/api/home-rooms")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(homeRoom)))
            .andExpect(status().isCreated());

        // Validate the HomeRoom in the database
        List<HomeRoom> homeRoomList = homeRoomRepository.findAll();
        assertThat(homeRoomList).hasSize(databaseSizeBeforeCreate + 1);
        HomeRoom testHomeRoom = homeRoomList.get(homeRoomList.size() - 1);
        assertThat(testHomeRoom.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testHomeRoom.getCode()).isEqualTo(DEFAULT_CODE);
    }

    @Test
    @Transactional
    public void createHomeRoomWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = homeRoomRepository.findAll().size();

        // Create the HomeRoom with an existing ID
        homeRoom.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restHomeRoomMockMvc.perform(post("/api/home-rooms")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(homeRoom)))
            .andExpect(status().isBadRequest());

        // Validate the HomeRoom in the database
        List<HomeRoom> homeRoomList = homeRoomRepository.findAll();
        assertThat(homeRoomList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllHomeRooms() throws Exception {
        // Initialize the database
        homeRoomRepository.saveAndFlush(homeRoom);

        // Get all the homeRoomList
        restHomeRoomMockMvc.perform(get("/api/home-rooms?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(homeRoom.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())));
    }
    
    @Test
    @Transactional
    public void getHomeRoom() throws Exception {
        // Initialize the database
        homeRoomRepository.saveAndFlush(homeRoom);

        // Get the homeRoom
        restHomeRoomMockMvc.perform(get("/api/home-rooms/{id}", homeRoom.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(homeRoom.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingHomeRoom() throws Exception {
        // Get the homeRoom
        restHomeRoomMockMvc.perform(get("/api/home-rooms/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateHomeRoom() throws Exception {
        // Initialize the database
        homeRoomRepository.saveAndFlush(homeRoom);

        int databaseSizeBeforeUpdate = homeRoomRepository.findAll().size();

        // Update the homeRoom
        HomeRoom updatedHomeRoom = homeRoomRepository.findById(homeRoom.getId()).get();
        // Disconnect from session so that the updates on updatedHomeRoom are not directly saved in db
        em.detach(updatedHomeRoom);
        updatedHomeRoom
            .name(UPDATED_NAME)
            .code(UPDATED_CODE);

        restHomeRoomMockMvc.perform(put("/api/home-rooms")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedHomeRoom)))
            .andExpect(status().isOk());

        // Validate the HomeRoom in the database
        List<HomeRoom> homeRoomList = homeRoomRepository.findAll();
        assertThat(homeRoomList).hasSize(databaseSizeBeforeUpdate);
        HomeRoom testHomeRoom = homeRoomList.get(homeRoomList.size() - 1);
        assertThat(testHomeRoom.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testHomeRoom.getCode()).isEqualTo(UPDATED_CODE);
    }

    @Test
    @Transactional
    public void updateNonExistingHomeRoom() throws Exception {
        int databaseSizeBeforeUpdate = homeRoomRepository.findAll().size();

        // Create the HomeRoom

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHomeRoomMockMvc.perform(put("/api/home-rooms")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(homeRoom)))
            .andExpect(status().isBadRequest());

        // Validate the HomeRoom in the database
        List<HomeRoom> homeRoomList = homeRoomRepository.findAll();
        assertThat(homeRoomList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteHomeRoom() throws Exception {
        // Initialize the database
        homeRoomRepository.saveAndFlush(homeRoom);

        int databaseSizeBeforeDelete = homeRoomRepository.findAll().size();

        // Delete the homeRoom
        restHomeRoomMockMvc.perform(delete("/api/home-rooms/{id}", homeRoom.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<HomeRoom> homeRoomList = homeRoomRepository.findAll();
        assertThat(homeRoomList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(HomeRoom.class);
        HomeRoom homeRoom1 = new HomeRoom();
        homeRoom1.setId(1L);
        HomeRoom homeRoom2 = new HomeRoom();
        homeRoom2.setId(homeRoom1.getId());
        assertThat(homeRoom1).isEqualTo(homeRoom2);
        homeRoom2.setId(2L);
        assertThat(homeRoom1).isNotEqualTo(homeRoom2);
        homeRoom1.setId(null);
        assertThat(homeRoom1).isNotEqualTo(homeRoom2);
    }
}
