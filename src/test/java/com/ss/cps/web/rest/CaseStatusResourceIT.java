package com.ss.cps.web.rest;

import com.ss.cps.CpsApp;
import com.ss.cps.domain.CaseStatus;
import com.ss.cps.repository.CaseStatusRepository;
import com.ss.cps.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.ss.cps.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CaseStatusResource} REST controller.
 */
@SpringBootTest(classes = CpsApp.class)
public class CaseStatusResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    @Autowired
    private CaseStatusRepository caseStatusRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restCaseStatusMockMvc;

    private CaseStatus caseStatus;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CaseStatusResource caseStatusResource = new CaseStatusResource(caseStatusRepository);
        this.restCaseStatusMockMvc = MockMvcBuilders.standaloneSetup(caseStatusResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CaseStatus createEntity(EntityManager em) {
        CaseStatus caseStatus = new CaseStatus()
            .name(DEFAULT_NAME)
            .code(DEFAULT_CODE);
        return caseStatus;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CaseStatus createUpdatedEntity(EntityManager em) {
        CaseStatus caseStatus = new CaseStatus()
            .name(UPDATED_NAME)
            .code(UPDATED_CODE);
        return caseStatus;
    }

    @BeforeEach
    public void initTest() {
        caseStatus = createEntity(em);
    }

    @Test
    @Transactional
    public void createCaseStatus() throws Exception {
        int databaseSizeBeforeCreate = caseStatusRepository.findAll().size();

        // Create the CaseStatus
        restCaseStatusMockMvc.perform(post("/api/case-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(caseStatus)))
            .andExpect(status().isCreated());

        // Validate the CaseStatus in the database
        List<CaseStatus> caseStatusList = caseStatusRepository.findAll();
        assertThat(caseStatusList).hasSize(databaseSizeBeforeCreate + 1);
        CaseStatus testCaseStatus = caseStatusList.get(caseStatusList.size() - 1);
        assertThat(testCaseStatus.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCaseStatus.getCode()).isEqualTo(DEFAULT_CODE);
    }

    @Test
    @Transactional
    public void createCaseStatusWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = caseStatusRepository.findAll().size();

        // Create the CaseStatus with an existing ID
        caseStatus.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCaseStatusMockMvc.perform(post("/api/case-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(caseStatus)))
            .andExpect(status().isBadRequest());

        // Validate the CaseStatus in the database
        List<CaseStatus> caseStatusList = caseStatusRepository.findAll();
        assertThat(caseStatusList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllCaseStatuses() throws Exception {
        // Initialize the database
        caseStatusRepository.saveAndFlush(caseStatus);

        // Get all the caseStatusList
        restCaseStatusMockMvc.perform(get("/api/case-statuses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(caseStatus.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())));
    }
    
    @Test
    @Transactional
    public void getCaseStatus() throws Exception {
        // Initialize the database
        caseStatusRepository.saveAndFlush(caseStatus);

        // Get the caseStatus
        restCaseStatusMockMvc.perform(get("/api/case-statuses/{id}", caseStatus.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(caseStatus.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCaseStatus() throws Exception {
        // Get the caseStatus
        restCaseStatusMockMvc.perform(get("/api/case-statuses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCaseStatus() throws Exception {
        // Initialize the database
        caseStatusRepository.saveAndFlush(caseStatus);

        int databaseSizeBeforeUpdate = caseStatusRepository.findAll().size();

        // Update the caseStatus
        CaseStatus updatedCaseStatus = caseStatusRepository.findById(caseStatus.getId()).get();
        // Disconnect from session so that the updates on updatedCaseStatus are not directly saved in db
        em.detach(updatedCaseStatus);
        updatedCaseStatus
            .name(UPDATED_NAME)
            .code(UPDATED_CODE);

        restCaseStatusMockMvc.perform(put("/api/case-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCaseStatus)))
            .andExpect(status().isOk());

        // Validate the CaseStatus in the database
        List<CaseStatus> caseStatusList = caseStatusRepository.findAll();
        assertThat(caseStatusList).hasSize(databaseSizeBeforeUpdate);
        CaseStatus testCaseStatus = caseStatusList.get(caseStatusList.size() - 1);
        assertThat(testCaseStatus.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCaseStatus.getCode()).isEqualTo(UPDATED_CODE);
    }

    @Test
    @Transactional
    public void updateNonExistingCaseStatus() throws Exception {
        int databaseSizeBeforeUpdate = caseStatusRepository.findAll().size();

        // Create the CaseStatus

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCaseStatusMockMvc.perform(put("/api/case-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(caseStatus)))
            .andExpect(status().isBadRequest());

        // Validate the CaseStatus in the database
        List<CaseStatus> caseStatusList = caseStatusRepository.findAll();
        assertThat(caseStatusList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCaseStatus() throws Exception {
        // Initialize the database
        caseStatusRepository.saveAndFlush(caseStatus);

        int databaseSizeBeforeDelete = caseStatusRepository.findAll().size();

        // Delete the caseStatus
        restCaseStatusMockMvc.perform(delete("/api/case-statuses/{id}", caseStatus.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CaseStatus> caseStatusList = caseStatusRepository.findAll();
        assertThat(caseStatusList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CaseStatus.class);
        CaseStatus caseStatus1 = new CaseStatus();
        caseStatus1.setId(1L);
        CaseStatus caseStatus2 = new CaseStatus();
        caseStatus2.setId(caseStatus1.getId());
        assertThat(caseStatus1).isEqualTo(caseStatus2);
        caseStatus2.setId(2L);
        assertThat(caseStatus1).isNotEqualTo(caseStatus2);
        caseStatus1.setId(null);
        assertThat(caseStatus1).isNotEqualTo(caseStatus2);
    }
}
