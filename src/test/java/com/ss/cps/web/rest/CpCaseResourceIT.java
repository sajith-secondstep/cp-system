package com.ss.cps.web.rest;

import com.ss.cps.CpsApp;
import com.ss.cps.domain.CpCase;
import com.ss.cps.repository.CpCaseRepository;
import com.ss.cps.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.ss.cps.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CpCaseResource} REST controller.
 */
@SpringBootTest(classes = CpsApp.class)
public class CpCaseResourceIT {

    private static final String DEFAULT_SCHOOL_NAME = "AAAAAAAAAA";
    private static final String UPDATED_SCHOOL_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_SCHOOL_ID = 1;
    private static final Integer UPDATED_SCHOOL_ID = 2;
    private static final Integer SMALLER_SCHOOL_ID = 1 - 1;

    private static final String DEFAULT_STUDENT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_STUDENT_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_STUDENT_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_STUDENT_NUMBER = "BBBBBBBBBB";

    private static final Integer DEFAULT_GENDER = 1;
    private static final Integer UPDATED_GENDER = 2;
    private static final Integer SMALLER_GENDER = 1 - 1;

    private static final String DEFAULT_ACADEMIC_YEAR = "AAAAAAAAAA";
    private static final String UPDATED_ACADEMIC_YEAR = "BBBBBBBBBB";

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    private static final String DEFAULT_REPORTER_NAME = "AAAAAAAAAA";
    private static final String UPDATED_REPORTER_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CLUSTER = "AAAAAAAAAA";
    private static final String UPDATED_CLUSTER = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_PHONE_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_INCIDENT_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_INCIDENT_TEXT = "BBBBBBBBBB";

    private static final String DEFAULT_INCIDENT_TYPE_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_INCIDENT_TYPE_TEXT = "BBBBBBBBBB";

    private static final String DEFAULT_GRADE_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_GRADE_TEXT = "BBBBBBBBBB";

    private static final String DEFAULT_HOME_ROOM_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_HOME_ROOM_TEXT = "BBBBBBBBBB";

    private static final String DEFAULT_RELATIONSHIP_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_RELATIONSHIP_TEXT = "BBBBBBBBBB";

    private static final String DEFAULT_OFFICER = "AAAAAAAAAA";
    private static final String UPDATED_OFFICER = "BBBBBBBBBB";

    private static final Integer DEFAULT_INCIDENT_COUNTER = 1;
    private static final Integer UPDATED_INCIDENT_COUNTER = 2;
    private static final Integer SMALLER_INCIDENT_COUNTER = 1 - 1;

    @Autowired
    private CpCaseRepository cpCaseRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restCpCaseMockMvc;

    private CpCase cpCase;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CpCaseResource cpCaseResource = new CpCaseResource(cpCaseRepository);
        this.restCpCaseMockMvc = MockMvcBuilders.standaloneSetup(cpCaseResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CpCase createEntity(EntityManager em) {
        CpCase cpCase = new CpCase()
            .schoolName(DEFAULT_SCHOOL_NAME)
            .schoolId(DEFAULT_SCHOOL_ID)
            .studentName(DEFAULT_STUDENT_NAME)
            .studentNumber(DEFAULT_STUDENT_NUMBER)
            .gender(DEFAULT_GENDER)
            .academicYear(DEFAULT_ACADEMIC_YEAR)
            .note(DEFAULT_NOTE)
            .reporterName(DEFAULT_REPORTER_NAME)
            .cluster(DEFAULT_CLUSTER)
            .email(DEFAULT_EMAIL)
            .phoneNumber(DEFAULT_PHONE_NUMBER)
            .incidentText(DEFAULT_INCIDENT_TEXT)
            .incidentTypeText(DEFAULT_INCIDENT_TYPE_TEXT)
            .gradeText(DEFAULT_GRADE_TEXT)
            .homeRoomText(DEFAULT_HOME_ROOM_TEXT)
            .relationshipText(DEFAULT_RELATIONSHIP_TEXT)
            .officer(DEFAULT_OFFICER)
            .incidentCounter(DEFAULT_INCIDENT_COUNTER);
        return cpCase;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CpCase createUpdatedEntity(EntityManager em) {
        CpCase cpCase = new CpCase()
            .schoolName(UPDATED_SCHOOL_NAME)
            .schoolId(UPDATED_SCHOOL_ID)
            .studentName(UPDATED_STUDENT_NAME)
            .studentNumber(UPDATED_STUDENT_NUMBER)
            .gender(UPDATED_GENDER)
            .academicYear(UPDATED_ACADEMIC_YEAR)
            .note(UPDATED_NOTE)
            .reporterName(UPDATED_REPORTER_NAME)
            .cluster(UPDATED_CLUSTER)
            .email(UPDATED_EMAIL)
            .phoneNumber(UPDATED_PHONE_NUMBER)
            .incidentText(UPDATED_INCIDENT_TEXT)
            .incidentTypeText(UPDATED_INCIDENT_TYPE_TEXT)
            .gradeText(UPDATED_GRADE_TEXT)
            .homeRoomText(UPDATED_HOME_ROOM_TEXT)
            .relationshipText(UPDATED_RELATIONSHIP_TEXT)
            .officer(UPDATED_OFFICER)
            .incidentCounter(UPDATED_INCIDENT_COUNTER);
        return cpCase;
    }

    @BeforeEach
    public void initTest() {
        cpCase = createEntity(em);
    }

    @Test
    @Transactional
    public void createCpCase() throws Exception {
        int databaseSizeBeforeCreate = cpCaseRepository.findAll().size();

        // Create the CpCase
        restCpCaseMockMvc.perform(post("/api/cp-cases")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cpCase)))
            .andExpect(status().isCreated());

        // Validate the CpCase in the database
        List<CpCase> cpCaseList = cpCaseRepository.findAll();
        assertThat(cpCaseList).hasSize(databaseSizeBeforeCreate + 1);
        CpCase testCpCase = cpCaseList.get(cpCaseList.size() - 1);
        assertThat(testCpCase.getSchoolName()).isEqualTo(DEFAULT_SCHOOL_NAME);
        assertThat(testCpCase.getSchoolId()).isEqualTo(DEFAULT_SCHOOL_ID);
        assertThat(testCpCase.getStudentName()).isEqualTo(DEFAULT_STUDENT_NAME);
        assertThat(testCpCase.getStudentNumber()).isEqualTo(DEFAULT_STUDENT_NUMBER);
        assertThat(testCpCase.getGender()).isEqualTo(DEFAULT_GENDER);
        assertThat(testCpCase.getAcademicYear()).isEqualTo(DEFAULT_ACADEMIC_YEAR);
        assertThat(testCpCase.getNote()).isEqualTo(DEFAULT_NOTE);
        assertThat(testCpCase.getReporterName()).isEqualTo(DEFAULT_REPORTER_NAME);
        assertThat(testCpCase.getCluster()).isEqualTo(DEFAULT_CLUSTER);
        assertThat(testCpCase.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testCpCase.getPhoneNumber()).isEqualTo(DEFAULT_PHONE_NUMBER);
        assertThat(testCpCase.getIncidentText()).isEqualTo(DEFAULT_INCIDENT_TEXT);
        assertThat(testCpCase.getIncidentTypeText()).isEqualTo(DEFAULT_INCIDENT_TYPE_TEXT);
        assertThat(testCpCase.getGradeText()).isEqualTo(DEFAULT_GRADE_TEXT);
        assertThat(testCpCase.getHomeRoomText()).isEqualTo(DEFAULT_HOME_ROOM_TEXT);
        assertThat(testCpCase.getRelationshipText()).isEqualTo(DEFAULT_RELATIONSHIP_TEXT);
        assertThat(testCpCase.getOfficer()).isEqualTo(DEFAULT_OFFICER);
        assertThat(testCpCase.getIncidentCounter()).isEqualTo(DEFAULT_INCIDENT_COUNTER);
    }

    @Test
    @Transactional
    public void createCpCaseWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = cpCaseRepository.findAll().size();

        // Create the CpCase with an existing ID
        cpCase.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCpCaseMockMvc.perform(post("/api/cp-cases")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cpCase)))
            .andExpect(status().isBadRequest());

        // Validate the CpCase in the database
        List<CpCase> cpCaseList = cpCaseRepository.findAll();
        assertThat(cpCaseList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllCpCases() throws Exception {
        // Initialize the database
        cpCaseRepository.saveAndFlush(cpCase);

        // Get all the cpCaseList
        restCpCaseMockMvc.perform(get("/api/cp-cases?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cpCase.getId().intValue())))
            .andExpect(jsonPath("$.[*].schoolName").value(hasItem(DEFAULT_SCHOOL_NAME.toString())))
            .andExpect(jsonPath("$.[*].schoolId").value(hasItem(DEFAULT_SCHOOL_ID)))
            .andExpect(jsonPath("$.[*].studentName").value(hasItem(DEFAULT_STUDENT_NAME.toString())))
            .andExpect(jsonPath("$.[*].studentNumber").value(hasItem(DEFAULT_STUDENT_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER)))
            .andExpect(jsonPath("$.[*].academicYear").value(hasItem(DEFAULT_ACADEMIC_YEAR.toString())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())))
            .andExpect(jsonPath("$.[*].reporterName").value(hasItem(DEFAULT_REPORTER_NAME.toString())))
            .andExpect(jsonPath("$.[*].cluster").value(hasItem(DEFAULT_CLUSTER.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].phoneNumber").value(hasItem(DEFAULT_PHONE_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].incidentText").value(hasItem(DEFAULT_INCIDENT_TEXT.toString())))
            .andExpect(jsonPath("$.[*].incidentTypeText").value(hasItem(DEFAULT_INCIDENT_TYPE_TEXT.toString())))
            .andExpect(jsonPath("$.[*].gradeText").value(hasItem(DEFAULT_GRADE_TEXT.toString())))
            .andExpect(jsonPath("$.[*].homeRoomText").value(hasItem(DEFAULT_HOME_ROOM_TEXT.toString())))
            .andExpect(jsonPath("$.[*].relationshipText").value(hasItem(DEFAULT_RELATIONSHIP_TEXT.toString())))
            .andExpect(jsonPath("$.[*].officer").value(hasItem(DEFAULT_OFFICER.toString())))
            .andExpect(jsonPath("$.[*].incidentCounter").value(hasItem(DEFAULT_INCIDENT_COUNTER)));
    }
    
    @Test
    @Transactional
    public void getCpCase() throws Exception {
        // Initialize the database
        cpCaseRepository.saveAndFlush(cpCase);

        // Get the cpCase
        restCpCaseMockMvc.perform(get("/api/cp-cases/{id}", cpCase.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(cpCase.getId().intValue()))
            .andExpect(jsonPath("$.schoolName").value(DEFAULT_SCHOOL_NAME.toString()))
            .andExpect(jsonPath("$.schoolId").value(DEFAULT_SCHOOL_ID))
            .andExpect(jsonPath("$.studentName").value(DEFAULT_STUDENT_NAME.toString()))
            .andExpect(jsonPath("$.studentNumber").value(DEFAULT_STUDENT_NUMBER.toString()))
            .andExpect(jsonPath("$.gender").value(DEFAULT_GENDER))
            .andExpect(jsonPath("$.academicYear").value(DEFAULT_ACADEMIC_YEAR.toString()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()))
            .andExpect(jsonPath("$.reporterName").value(DEFAULT_REPORTER_NAME.toString()))
            .andExpect(jsonPath("$.cluster").value(DEFAULT_CLUSTER.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.phoneNumber").value(DEFAULT_PHONE_NUMBER.toString()))
            .andExpect(jsonPath("$.incidentText").value(DEFAULT_INCIDENT_TEXT.toString()))
            .andExpect(jsonPath("$.incidentTypeText").value(DEFAULT_INCIDENT_TYPE_TEXT.toString()))
            .andExpect(jsonPath("$.gradeText").value(DEFAULT_GRADE_TEXT.toString()))
            .andExpect(jsonPath("$.homeRoomText").value(DEFAULT_HOME_ROOM_TEXT.toString()))
            .andExpect(jsonPath("$.relationshipText").value(DEFAULT_RELATIONSHIP_TEXT.toString()))
            .andExpect(jsonPath("$.officer").value(DEFAULT_OFFICER.toString()))
            .andExpect(jsonPath("$.incidentCounter").value(DEFAULT_INCIDENT_COUNTER));
    }

    @Test
    @Transactional
    public void getNonExistingCpCase() throws Exception {
        // Get the cpCase
        restCpCaseMockMvc.perform(get("/api/cp-cases/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCpCase() throws Exception {
        // Initialize the database
        cpCaseRepository.saveAndFlush(cpCase);

        int databaseSizeBeforeUpdate = cpCaseRepository.findAll().size();

        // Update the cpCase
        CpCase updatedCpCase = cpCaseRepository.findById(cpCase.getId()).get();
        // Disconnect from session so that the updates on updatedCpCase are not directly saved in db
        em.detach(updatedCpCase);
        updatedCpCase
            .schoolName(UPDATED_SCHOOL_NAME)
            .schoolId(UPDATED_SCHOOL_ID)
            .studentName(UPDATED_STUDENT_NAME)
            .studentNumber(UPDATED_STUDENT_NUMBER)
            .gender(UPDATED_GENDER)
            .academicYear(UPDATED_ACADEMIC_YEAR)
            .note(UPDATED_NOTE)
            .reporterName(UPDATED_REPORTER_NAME)
            .cluster(UPDATED_CLUSTER)
            .email(UPDATED_EMAIL)
            .phoneNumber(UPDATED_PHONE_NUMBER)
            .incidentText(UPDATED_INCIDENT_TEXT)
            .incidentTypeText(UPDATED_INCIDENT_TYPE_TEXT)
            .gradeText(UPDATED_GRADE_TEXT)
            .homeRoomText(UPDATED_HOME_ROOM_TEXT)
            .relationshipText(UPDATED_RELATIONSHIP_TEXT)
            .officer(UPDATED_OFFICER)
            .incidentCounter(UPDATED_INCIDENT_COUNTER);

        restCpCaseMockMvc.perform(put("/api/cp-cases")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCpCase)))
            .andExpect(status().isOk());

        // Validate the CpCase in the database
        List<CpCase> cpCaseList = cpCaseRepository.findAll();
        assertThat(cpCaseList).hasSize(databaseSizeBeforeUpdate);
        CpCase testCpCase = cpCaseList.get(cpCaseList.size() - 1);
        assertThat(testCpCase.getSchoolName()).isEqualTo(UPDATED_SCHOOL_NAME);
        assertThat(testCpCase.getSchoolId()).isEqualTo(UPDATED_SCHOOL_ID);
        assertThat(testCpCase.getStudentName()).isEqualTo(UPDATED_STUDENT_NAME);
        assertThat(testCpCase.getStudentNumber()).isEqualTo(UPDATED_STUDENT_NUMBER);
        assertThat(testCpCase.getGender()).isEqualTo(UPDATED_GENDER);
        assertThat(testCpCase.getAcademicYear()).isEqualTo(UPDATED_ACADEMIC_YEAR);
        assertThat(testCpCase.getNote()).isEqualTo(UPDATED_NOTE);
        assertThat(testCpCase.getReporterName()).isEqualTo(UPDATED_REPORTER_NAME);
        assertThat(testCpCase.getCluster()).isEqualTo(UPDATED_CLUSTER);
        assertThat(testCpCase.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testCpCase.getPhoneNumber()).isEqualTo(UPDATED_PHONE_NUMBER);
        assertThat(testCpCase.getIncidentText()).isEqualTo(UPDATED_INCIDENT_TEXT);
        assertThat(testCpCase.getIncidentTypeText()).isEqualTo(UPDATED_INCIDENT_TYPE_TEXT);
        assertThat(testCpCase.getGradeText()).isEqualTo(UPDATED_GRADE_TEXT);
        assertThat(testCpCase.getHomeRoomText()).isEqualTo(UPDATED_HOME_ROOM_TEXT);
        assertThat(testCpCase.getRelationshipText()).isEqualTo(UPDATED_RELATIONSHIP_TEXT);
        assertThat(testCpCase.getOfficer()).isEqualTo(UPDATED_OFFICER);
        assertThat(testCpCase.getIncidentCounter()).isEqualTo(UPDATED_INCIDENT_COUNTER);
    }

    @Test
    @Transactional
    public void updateNonExistingCpCase() throws Exception {
        int databaseSizeBeforeUpdate = cpCaseRepository.findAll().size();

        // Create the CpCase

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCpCaseMockMvc.perform(put("/api/cp-cases")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cpCase)))
            .andExpect(status().isBadRequest());

        // Validate the CpCase in the database
        List<CpCase> cpCaseList = cpCaseRepository.findAll();
        assertThat(cpCaseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCpCase() throws Exception {
        // Initialize the database
        cpCaseRepository.saveAndFlush(cpCase);

        int databaseSizeBeforeDelete = cpCaseRepository.findAll().size();

        // Delete the cpCase
        restCpCaseMockMvc.perform(delete("/api/cp-cases/{id}", cpCase.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CpCase> cpCaseList = cpCaseRepository.findAll();
        assertThat(cpCaseList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CpCase.class);
        CpCase cpCase1 = new CpCase();
        cpCase1.setId(1L);
        CpCase cpCase2 = new CpCase();
        cpCase2.setId(cpCase1.getId());
        assertThat(cpCase1).isEqualTo(cpCase2);
        cpCase2.setId(2L);
        assertThat(cpCase1).isNotEqualTo(cpCase2);
        cpCase1.setId(null);
        assertThat(cpCase1).isNotEqualTo(cpCase2);
    }
}
